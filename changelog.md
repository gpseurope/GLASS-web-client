# Newest Release

(from old to recent)

## 1.0 - 29/09/2020

----------------------------------------- 

* Build tools have been updated (from bower and grunt to webpack)

* Fixed bug with the network selector (some network were impossible to interact with)

* The station are now ordered by name at the start, you can choose to change by clicking on the column name you want

* The stations names are now displayed with the 9 characters

* Fixed the zoom issues, you now need to press CTRL+SCROLL to zoom in or out. 

* Advanced search is now a bit more esthetic

## 2.0.0 03/12/2021

* update document
* menu modifications
* style modification
* upgrade ui-grid version
* modification Readme
* modification of treatment Rinex file array
* modification T3
* display data / calendar treatment
* Add tooltip header grid file
* add csv export to grid
* customize grid menu
* add csv and google analytics
* modification .gitignore
* style and new footer
* bug correction for stations with end dates. Adding new node configuration letter and node guidelines

## 2.1.0 20/09/2023

* delete jquery dependencie in package.json for fix error compilation no module jquery
* add config.template gitignore config.js
* no versioning for dist/
* add modal no dowload ftp links on download selected
* add https link for download documentation of CLC
* Switching from scope to modal, management of downloads by selection and direct clicks.
* add link to plots page in documentation, router of plots page done, add get http request  to receive plots
* upgrade leaflet to 1.9.3
* modification of command line client
* correct bug on inverse filter
* change gestureHandling for zoom and change leaflet version for drawing
* fix display map bug on multiple dataview
* change configuration file for Glass node configuration letter
* Style popup and modal
* Modal for team reduced with don't ask again in localStorage
* fix bugs of QC Charts

## 2.1.1 14/11/2023

* Stop display modal Team Reduced
* Change Readme and index menu
* Change Node ID IPGP
* Fix bug qualityChart (more graph than date) add condition with priority L2W > L2X ....


## 2.1.2 14/11/2023
 
*  New release with API call for retrieval and display of license DOI and stations pictures.


## 2.1.4 2024-02-02

* change on time series fetch address, add new time serie ROB-EUREF, change name time serie LTK in SGO-EPND, new style for display time serie name in carousel simple metadata page, correction of tour interface for node version and add information in config.js.template
* changing url for the product gateway
* The tour of the interface is fixed
* Fix a bug when diplaying data availability on the multiple station metadata page

## 2.1.5 2024-03-06

* merging missed changes from master
* adding a .npmrc file to be able to compile with recent nodejs
* Updating the README.md
* Adding a README_build.md file to explain how to build the web client from source.


## 2.2.0 2024-10-18 

* 9 chars markers are now used everywhere
* handle dateRange with only the start date for station and file queries (station query doesnt handle the end coma, and file query require it)
* links to pyglass are corrected
* correction of the pyglass export for circle selection and date range for file