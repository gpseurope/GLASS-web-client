# GLASS Web Client

**Version: 2.2.0** 18/10/2024

The Web client is a graphical user interface for the Data and Metadata provided by the **GLASS Framework** APIS.


**Author:**

* Guillaume Verbiese (guillaume.verbiese@oca.eu)

**Former Contributors**
* Khai-Minh NGO (Khai-Minh.NGO@geoazur.unice.fr)
* Arthur Fontaine (arthur.fontaine@geoazur.unice.fr)

## Installation

1. Download the war file "glasswebui.war" and the config.js file on Gitlab on the bottom

2. Open config.js and change the line : server:"http://\<IP\>:\<port\>/" with you server's address and port. Replace <mail> by the mail of the node contact

3. Open a terminal and execute the command:  "jar -uvf glasswebui.war config.js" to update in the war file with your new config.js

4. Deploy the war file on Glassfish or Payara, either by copying the war file to the autodeploy directory or by using the admin console.

5. Check the address : "http://\<IP\>:\<port\>/glasswebui"

6. On the bottom right the version number should appears


To rename the clientname:"glasswebui" by another one for example "french_node":

At step 4) before to deploy your war file, you have to rename also your war file "glasswebui.war" to "french_node.war"

and at step 5) Check with the browser "http://\<IP\>:\<port\>/french_node" 

## Modifying and building from source

See the file README_build.md

## Acknowledgments
* This project has received funding from the European Union's Horizon 2020 research and innovation programm under grant agreement N° 676564

#