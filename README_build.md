# Building from sources


You need to have **nodejs** and **npm** installed on your computer to build the application.



```bash
# Clone the repository for a specific tag
git clone -b <tags> git@gitlab.com:gpseurope/GLASS-web-client.git 
# go to the new directory
cd GLASS-web-client
# install the dependencies
npm install
# copy the template file to the config.js file
cp src/config.js.template src/config.js
# edit the config.js file
# specifically the IP and the port of the server
```

There are very few difference between the two versions of the web client, the local node client and the DGW client and the choice is made at the build time.

There are two files '*.config.js' which allow webpack to build the application for local node or for the Data Gateway. The config.js file load a different *index.html* where the *body* has a different *id*, and the logos are different. Inside the code, the differences are made with the function **$scope.isDgw()** but every files stays the same in both versions.


To build the code:
* For a local node GUI
  1. build the client: npm run build
  2. run on the development web server: npm run server

* DGW
  1. build the client:  npm run dgwBuild
  2. run on the development web server:: npm run dgwServer


to build the `.war` to be used in the Glassfish/Payara server:
```bash
npm run war # for the local node GUI
npm run dgwWar # for the DGW GUI
```
And deploy the file by copying it to the autodeploy directory of the server or by using the admin console.