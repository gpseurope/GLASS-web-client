const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');
const { config, cpuUsage } = require('process');
const CopyPlugin  = require('copy-webpack-plugin');

module.exports = {
  entry:{
    index:[
      './src/app/app.js',
      './src/app/controllers/file.js',
      './src/app/controllers/metadata.js',
      './src/app/controllers/network.js',
      './src/app/controllers/site.js',
      './src/app/controllers/help.js',
      './src/app/controllers/nodeinfo.js',
      './src/app/services/servicehistory.js',
      './src/app/services/serviceresource.js',
      './src/cookie.js'
    ],
    config:'./src/config.js'
  },
  mode: 'production', //production or development
  module: {
    rules:[
      { test: /\.css$/, use: ['style-loader','css-loader']},
      { test: /\.(png|jpe?g|gif)$/i, loader: 'file-loader', options: {outputPath: './images', name: '[name].[ext]'}},
      { test: /\.(woff(2)?|ttf|eot|svg)$/i, loader: 'file-loader', options:{ outputPath: './files', name: '[name].[ext]'}},
      { test: /\.svg$/, use: 'svg-inline-loader'}
    ]
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: './src/doc', to: './doc'},
        { from: './src/views', to: './views'},
        { from: './src/images', to: './images'},
        { from: './src/favicon.ico', to: '.'},
        { from: './src/404.html', to: '.'},
      ]
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      }
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jquery: "jquery",
      "window.jQuery": "jquery",
      jQuery:"jquery"
    })
  ],
  devServer:{
    contentBase: path.resolve(__dirname, "dist"),
    historyApiFallback: true,
    inline: true,
    open: true,
    hot: true,
    stats: 'minimal'
  },
  optimization:{
    minimize: false
  }, 
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    globalObject: 'this'
  }
};