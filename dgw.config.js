const HtmlWebpackPlugin = require('html-webpack-plugin'); //read html. installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path'); // manage paths
const { config, cpuUsage } = require('process');
const CopyPlugin  = require('copy-webpack-plugin');  // allows to move/copy files

module.exports = {
  entry:{
    index:[ // Bundle all js files in 2 files (index.js and 2.js), except config.js
      './src/app/app.js',
      './src/app/controllers/file.js',
      './src/app/controllers/metadata.js',
      './src/app/controllers/network.js',
      './src/app/controllers/site.js',
      './src/app/controllers/help.js',
      './src/app/controllers/nodeinfo.js',
      './src/app/controllers/plots.js',
      './src/app/services/servicehistory.js',
      './src/app/services/serviceresource.js',
      './src/cookie.js'
    ],
    config:'./src/config.js' // Bundle config.js alone
  },
  mode: 'production', //production or development
  module: {
    rules:[ // import loaders for all these kind of files
      { test: /\.css$/, use: ['style-loader','css-loader']},
      { test: /\.(png|jpe?g|gif)$/i, loader: 'file-loader', options: {outputPath: './images', name: '[name].[ext]'}},
      { test: /\.(woff(2)?|ttf|eot|svg)$/i, loader: 'file-loader', options:{ outputPath: './files', name: '[name].[ext]'}},
      { test: /\.svg$/, use: 'svg-inline-loader'}
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({ // allows to read/analyze html. The plugin will generate an HTML5 file for you that includes all your webpack bundles in the body using script tags
      template: './src/index_dgw.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      }
    }),
    new webpack.ProvidePlugin({ //  jquery has to be loaded independently from other js libraries
      $: "jquery",
      jquery: "jquery",
      "window.jQuery": "jquery",
      jQuery:"jquery"
    }),
    new CopyPlugin({
      patterns: [
        { from: './src/doc', to: './doc'},
        { from: './src/views', to: './views'},
        { from: './src/images', to: './images'},
        { from: './src/favicon.ico', to: '.'},
        { from: './src/404.html', to: '.'},
      ]
    })
  ],
  devServer:{
    contentBase: path.resolve(__dirname, "dist"),
    historyApiFallback: true,
    inline: true,
    open: true,
    hot: true,
    stats: 'minimal'
  },
  optimization:{
    minimize: false
  }, 
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dgw_dist'),
    globalObject: 'this'
  }
};