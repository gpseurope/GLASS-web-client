'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc overview
 * @name glasswebuiApp
 * @description
 * # glasswebuiApp
 *
 * Main module of the application.
 */


/**
app.js is the default file name for storing the functions which doesn't belong to the different views
*/

// IMPORT CSS
import "../../node_modules/bootstrap/dist/css/bootstrap.css"
import "../../node_modules/angular-tour/dist/angular-tour.css"
import "../../node_modules/jquery-ui/themes/base/base.css"
import "../../node_modules/jquery-ui/themes/base/slider.css"
import "../../node_modules/angular-ui-grid/ui-grid.css"
import "../../node_modules/leaflet/dist/leaflet.css"
import "../../node_modules/leaflet-draw/dist/leaflet.draw.css"
import "../../node_modules/angular-ui-layout/src/ui-layout.css"
import "../../node_modules/cal-heatmap/cal-heatmap.css"
import "../../node_modules/lightbox2/dist/css/lightbox.css"
import "../styles/index.css"
import "../styles/site.css"
import "../styles/metadata.css"
import "../styles/help.css"
import "../styles/nodeinfo.css"
import "../styles/plots.css"

//IMPORT JS
require('angular');
require('webpack-jquery-ui');
require('webpack-jquery-ui/css');
require('bootstrap');
require('angular-animate');
require('angular-cookies/angular-cookies');
require('angular-resource');
require('angular-route');
require('angular-sanitize');
require('angular-touch/index.js');
require('angular-bootstrap');
require('angular-strap');
require('angular-ui');
require('angular-filter');
require('angular-ui-grid/index');
require('leaflet');
require('leaflet-draw');
require('leaflet-gesture-handling');
import('pdfmake');
require('angular-ui-slider/src/index.js');
require('angular-ui-layout/index');
require('angular-tour/dist/angular-tour-tpls.js');
require('jszip');
require('jszip-utils/dist/jszip-utils');
require('lightbox2/dist/js/lightbox');
require('angular-ui-bootstrap/dist/ui-bootstrap-tpls');

var app = angular.module('glasswebuiApp', [
	'ngAnimate',
	'ngCookies',
	'ngResource',
	'ngRoute',
	'ngSanitize',
	'ngTouch',
	'ui.bootstrap',
	'angular.filter',
	'ui.grid',
	'ui.grid.selection',
	'ui.grid.exporter',
	'ui.grid.autoResize',
	'ui.grid.moveColumns',
	'ui.grid.resizeColumns',
	'mgcrea.ngStrap',
	'mgcrea.ngStrap.datepicker',
	'ui.slider',
	'mgcrea.ngStrap.tab',
	'ui.layout',
	'angular-tour',
	'ui.bootstrap'
]);

app.config(function ($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix(''); // needed by the latest angularJS to make the routing works
	$routeProvider
		.when('/site', {
			templateUrl: 'views/site.html',
			controller: 'SiteCtrl',
			controllerAs: 'site'
		})
		.when('/metadata/:querystring*', {
			templateUrl: 'views/metadata.html',
			controller: 'MetadataCtrl',
			controllerAs: 'metadata'
		})
		.when('/file/:querystring*', {
			templateUrl: 'views/file.html',
			controller: 'FileCtrl',
			controllerAs: 'file'
		})
		.when('/network/:querystring*', {
			templateUrl: 'views/network.html',
			controller: 'NetworkCtrl',
			controllerAs: 'network'
		})
		.when('/help', {
			templateUrl: 'views/help.html',
			controller: 'HelpCtrl',
			controllerAs: 'help'
		})
		.when('/nodeInfo', {
			templateUrl: 'views/nodeInfo.html',
			controller: 'NodeCtrl',
			controllerAs: 'nodeinfo'
		})
		.when('/plots', {
			templateUrl: 'views/plots.html',
			controller: 'PlotsCtrl',
			controllerAs: 'plots',
			resolve: {
				dgw: redirect
			}
		})
		.otherwise({
			redirectTo: '/site'
		});
});

var redirect = function ($location) {
	if (document.body.id == "dgw") {
		return true;
	} else {
		$location.path("/site");
	}
}


// app.run start automatically at the beginning
app.run(function ($rootScope, $window, $cookies, $uibModal, serviceHistory, $timeout, $http, CONFIG) {

	$rootScope.version = "2.2.0";

	/**************************************************
	 * routeLogin function allows to handle authentification  
	 ***************************************************/
	$rootScope.routeLogin = function () {
		alert("Login");
	};
	/*******************************************
	 * routeHistory function allows to handle history  
	 *******************************************/
	$rootScope.routeHistory = function () {
		var cookies = $cookies.getAll();
		$rootScope.historydata = [];
		angular.forEach(cookies, function (v, k) {
			var myDateStr = new Date(k);
			if (!isNaN(myDateStr.getMonth())) {
				$rootScope.historydata.push(v);
			}
		});
		$rootScope.opendialog();
	};


	/******************************************************************************************
	 * opendialog function allows to open a modal when the users click on "request query" button 
	 *******************************************************************************************/
	$rootScope.opendialog = function () {
		return $rootScope.modalInstance = $uibModal.open({
			templateUrl: 'views/modalHistory.html',
			size: 'lg',
			scope: $rootScope,
			windowClass: 'center-modal'
		});
	};

	/***************************************
	 * cancel function allows to cancel modal
	 ****************************************/
	$rootScope.cancel = function () {
		$rootScope.modalInstance.dismiss("cancel");
	};

	/*******************************************************
	 * downloadText function allows to download text on modal
	 *******************************************************/
	$rootScope.downloadText = function () {
		var mytext = $('#divid').text();
		var mystring = String(mytext);
		var out = mystring.replace(/http/g, '\n http');
		var blob = new Blob([out], {
			type: 'text/plain'
		});
		if (window.navigator.msSaveOrOpenBlob) { //IE
			window.navigator.msSaveBlob(blob, "history.txt");
		} else {
			var elem = window.document.createElement('a');
			elem.href = window.URL.createObjectURL(blob);
			elem.download = "history.txt";
			document.body.appendChild(elem);
			elem.click();
			document.body.removeChild(elem);
		}
	};

	/***********************************************
	 * clearhistory function allows to clear history
	 ***********************************************/
	$rootScope.clearhistory = function () {
		$("#divid").html("");
		serviceHistory.clearhistory(); //delete the cookie
	};

	/***********************************************
	 * help function allows to show demonstrate tour
	 ***********************************************/
	$rootScope.tour = function () { // initiate the start of the tour
		$rootScope.currentStep = 0;
		$timeout(function () {
			$('#myselector').triggerHandler('click');
		}, 0);
	};

	/********************************************
	 * tutoriel function allows to show the video
	 ********************************************/
	$rootScope.tutoriel = function () {
		return $rootScope.modalInstance = $uibModal.open({
			templateUrl: 'views/modalVideo.html',
			size: 'lg',
			scope: $rootScope,
			windowClass: 'center-modal'
		});
	};
	/********************************************
	 * doc function allows to show the document
	 ********************************************/
	$rootScope.doc = function () {
		return $rootScope.modalInstance = $uibModal.open({
			templateUrl: 'views/modalDoc.html',
			size: 'lg',
			scope: $rootScope,
			windowClass: 'center-modal'
		});
	};

	/**
	 * isDgw() return boolean to know which version is running
	 */
	$rootScope.isDgw = function () {
		return document.body.id == "dgw";
	}
	/**
	 * getMail() put the mail specified in config.js inside the "contact" link in the index's footer
	 */
	$rootScope.getMail = function () {
		document.getElementById('mail').href = CONFIG.mailto;
	}

	/**
	 * commandLineClient() open the modal containing the link to download
	 */
	$rootScope.commandLineClient = function () {
		return $rootScope.modalInstance = $uibModal.open({
			templateUrl: 'views/modalCommandLineClient.html',
			size: 'lg',
			scope: $rootScope,
			windowClass: 'center-modal'
		});
	}

	/**
	 * noDownload(), open the modal containing the information on the inability to download the files
	 */

	$rootScope.noDownload = function () {
		return $rootScope.modalInstance = $uibModal.open({
			templateUrl: 'views/modalNoDownload.html',
			size: 'lg',
			scope: $rootScope,
			windowClass: 'center-modal'
		})
	}
});

/****************************************************************************************
 * removeSpaces filter allows to delete all spaces in network name for id in input element
 ****************************************************************************************/
// code potentially unused now. don't delete before carefully checking

app.filter('removeSpaces', [function () {
	return function (string) {
		if (!angular.isString(string)) {
			return string;
		}
		string = string.replace(/&/g, '');
		string = string.replace('.', '');
		string = string.replace('/', '');
		if (string.includes("NFO_")) {
			string = "NFO"
		}
		return string.replace(/[\s]/g, '');
	};
}]);

