'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc service
 * @name glasswebuiApp.serviceResource
 * @description
 * # serviceResource
 * Factory in the glasswebuiApp.
 */

// service for "static" endpoints

angular.module('glasswebuiApp')
  .factory('serviceResource', function serviceResource($http, $resource, CONFIG) {
    var server = CONFIG.server;
    var clientname = CONFIG.clientname;
    var glassapi = CONFIG.glassapi;


    return {
      site: function () {
        //return $resource(server + glassapi + "/webresources/stations/stations-short");
        //return $resource(server + glassapi + "/webresources/stations/v2/station/short/json");
        return $resource(server + glassapi + "/webresources/stations/v2/combination/short/json");
      },
      receiver: function () {
        //return $resource(server + glassapi + "/webresources/stations/receiver-type-list");
        return $resource(server + glassapi + "/webresources/stations/v2/list/receiver");
      },
      antenna: function () {
        //return $resource(server + glassapi + "/webresources/stations/antenna-type-list");
        return $resource(server + glassapi + "/webresources/stations/v2/list/antenna");
      },
      radome: function () {
        //return $resource(server + glassapi + "/webresources/stations/radome-type-list");
        return $resource(server + glassapi + "/webresources/stations/v2/list/radome");
      },
      filetype: function () {
        //return $resource(server + glassapi + "/webresources/stations/files/file-type-list");
        return $resource(server + glassapi + "/webresources/stations/v2/list/files_type");
      },
      nodeinfo: function () {
        return $resource(server + glassapi + "/webresources/t0-manager/getNodes");
      }
    };
  });
