'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc service
 * @name glasswebuiApp.serviceHistory
 * @description
 * # serviceHistory
 * Factory in the glasswebuiApp.
 */
angular.module('glasswebuiApp')
  .factory('serviceHistory', function serviceHistory($cookies) {
    return{
	addhistory : function(data) {
            var cookies = $cookies.getAll();
            var exist = false;
            angular.forEach(cookies, function (v, k) {
                if (v===data){
			exist = true;
		}
	    });
            
	    if(exist===false){
		$cookies.put(new Date(), data);
	    }
	},
	clearhistory : function() {
	    var cookies = $cookies.getAll();
	    angular.forEach(cookies, function (v, k) {
                    $cookies.remove(k);	    
	    });
	}	
    };
    
  });
