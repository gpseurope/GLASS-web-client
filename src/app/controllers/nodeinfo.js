'use strict';
const { circleMarker, Marker, marker } = require("leaflet");
const { sub } = require("angular-route");
const { scaleBand } = require("d3");

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 *   Arthur Fontaine <arthur.fontaine@geoazur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:NodeCtrl
 * @description
 * # NodeCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
    .controller('NodeCtrl', function ($scope, serviceHistory, uiGridConstants, serviceResource, $uibModal, $window, $resource, $routeParams, $templateCache, CONFIG, $http, $location, $q, /*$cookie,*/ $interval, $timeout, NODE) {
        var nodeGpsList = {
            //nodeName : [lat, lon, address, [nodeID]]
            'Belgian EPOS-GNSS node (Belgian-node)': ['50.7985403', '4.358779', "Av. Circulaire 3, 1180 Uccle", [23]],
            'Central and East European GNSS Node for EPOS': ['41.8281162', '12.5151489', ' ', [25]],
            // 'CzechGeo': ['50.1667', '14.4333', "Ústecká 98 250 66 Zdiby", [4]],
            'GNSS Permanent IPGP Network (IPGP-GNSS)': ['48.8447757', '2.3561337', "1 Rue Jussieu, 75005 Paris", [22]],
            'EPOS GNSS Data Gateway (DGW) & French National Node (French-Node)': ['43.612047', '7.0556473', "250 rue Albert Einstein,06560 Valbonne", [1,2]],
            'INGV RING NODE (IRN)': ['41.8281162', '12.5151489', "Via di Vigna Murata 605 00143 Roma", [11]],
            'National Observatory of Athens (NOA)': ['37.9732993', '23.7180479', "Lofos Nymfon, Thission 11810 Athens", [10]],
            'ROB-EUREF': ['50.7985403', '4.358779', "Av. Circulaire 3, 1180 Uccle", [5]],
            'Romanian National Node (NIEP GNSS)': ['44.35', '26.03333', "Strada Călugăreni 12, Măgurele 077125", [9]],
            'SONEL EPOS-GNSS at tide gauges node (SONEL)': ['46.1470786','-1.1667498,', "12 rue Olympe de Gouges F-17042 LA ROCHELLE Cedex", [28]],
            'Spanish EPOS Data node': ['40.44532775878906', '-3.7105207443237305', "C/ General Ibáñez de Íbero 3, 28003 Madrid", [18]],
            'Portuguese EPOS National Node & EPOS-GNSS Pan-European Node': ['40.2781456', '-7.5083823', "Universidade da Beira Interior – Departamento de Informática – Caminho Biribau – 6201-001 Covilhã", [8,14]],
        };
        $scope.markers = new L.FeatureGroup();

        //load the map
        //$scope.mymap = L.map('mapid').setView([51.505, -0.09], 3);
        $scope.mymap = L.map('nodemap', {
            center: [48, 5],
            zoom: 4,
            zoomDelta: 0.5,
            minZoom: 2,
            gestureHandling: true
        });
        L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
        //Scroll  with CTRL+Mouse method
        $scope.mymap.scrollWheelZoom.disable();
        $('#mapid').bind('mousewheel DOMMouseScroll', function (event) {
            event.stopPropagation();
            if (event.ctrlKey == true) {
                event.preventDefault();
                $scope.mymap.scrollWheelZoom.enable();
                setTimeout(function () {
                    $scope.mymap.scrollWheelZoom.disable();
                }, 2000);
            } else {
                $scope.mymap.scrollWheelZoom.disable();
            }
        });

        var query = serviceResource.nodeinfo().query();
        //$scope.loadingdialog();
        query.$promise.then(function (data) {
            var result = data;
            var optionMarker = { radius: 7, fillColor: "#FF0000", color: "#000", weight: 1, opacity: 1, fillOpacity: 0.8 };
            for (var i = 0; i < Object.keys(nodeGpsList).length; i++) {
                var container = $('<div />'); //create a container for popup
                var marker = new L.circleMarker([nodeGpsList[Object.keys(nodeGpsList)[i]][0], nodeGpsList[Object.keys(nodeGpsList)[i]][1]], optionMarker)
                var htmlLink = ''
                for(y=0; y<nodeGpsList[Object.keys(nodeGpsList)[i]][3].length;y++){
                    htmlLink += '</span><br style="line-height:0px;"><span><b>Link: </b><a href="' + $scope.getNodeInfo(nodeGpsList[Object.keys(nodeGpsList)[i]][3][y], result, "NodeUrl")+ '">' + $scope.getNodeInfo(nodeGpsList[Object.keys(nodeGpsList)[i]][3][y], result, "NodeName") + '</a>';
                }
                container.html('<span><b>Name:</b> ' + Object.keys(nodeGpsList)[i] +
                    '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + nodeGpsList[Object.keys(nodeGpsList)[i]][0] +
                    '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + nodeGpsList[Object.keys(nodeGpsList)[i]][1] +
                    '</span><br style="line-height:0px;"><span><b>Address:</b> ' + nodeGpsList[Object.keys(nodeGpsList)[i]][2]+htmlLink);
                marker.bindPopup(container[0]);
                $scope.markers.addLayer(marker);
            }
            $scope.mymap.addLayer($scope.markers);

            //Set the table urls
            var nodes = document.getElementsByClassName("node");
            for(var i = 0; i < nodes.length; i++){
                var id = "";
                var name = nodes[i].innerHTML;
                if (['EPOS GNSS Data Gateway (DGW)', 'French National Node (French-Node)'].includes(name))
                    if(name == "DGW")
                        id = nodeGpsList["EPOS GNSS Data Gateway (DGW) & French National Node (French-Node)"][3][0];
                    else
                        id = nodeGpsList["EPOS GNSS Data Gateway (DGW) & French National Node (French-Node)"][3][1];
                else if (['Portuguese EPOS National Node', 'EPOS-GNSS Pan-European Node'].includes(name))
                    if(name == "EPOS-GNSS Pan-European Node")
                        id = nodeGpsList["Portuguese EPOS National Node & EPOS-GNSS Pan-European Node"][3][1];
                    else
                        id = nodeGpsList["Portuguese EPOS National Node & EPOS-GNSS Pan-European Node"][3][0];
                else
                    id = nodeGpsList[name][3][0]
                
                nodes[i].href = $scope.getNodeInfo(id, result, "NodeUrl")
            }
        });

        //Set up the doc
        var container = document.getElementById("node_doc");
        var lettercontainer = document.createElement("div");
        var subtitle = document.createElement("p"); subtitle.classList.add("subtitleHelp"); subtitle.innerText = "Documents"; container.appendChild(subtitle);
        for (var y = 0; y < NODE.table.length; y++) {
            if (NODE.table[y][2] != "") {
                var tab = document.createElement("div"); tab.classList.add("tab");
                var value = document.createElement("p"); value.innerText = NODE.table[y][0];
                tab.appendChild(value);
                value = document.createElement("p"); value.innerText = NODE.table[y][1];
                tab.appendChild(value);
                value = document.createElement("p");
                var link = document.createElement("a"); link.innerText = "Download"; link.href = NODE.table[y][2]; link.target = "_blank"; value.appendChild(link);
                tab.appendChild(value);
                lettercontainer.appendChild(tab);
            }
        }
        container.appendChild(lettercontainer);
        var br = document.createElement("br"); container.appendChild(br);

        if(window.location.hash.includes('BecomeNode')){
            document.getElementById('BecomeNode').scrollIntoView();
        }

        
        /**
         * @var NodeID, result, item
         * @returns json object
         */
        $scope.getNodeInfo = function (nodeID, result, item) {
            for(var i = 0; i < result.length; i++){
                if(result[i]['NodeID'] == nodeID) return result[i][item];
            }
            return 'Not Found'
        }


        /*************************************************************
         * loadingdialog function allows to open a modal with a spinner 
         **************************************************************/
        $scope.loadingdialog = function () {
            return $scope.modalInstance = $uibModal.open({
                templateUrl: 'views/modalLoading.html',
                size: 'm',
                scope: $scope,
                windowClass: 'center-modal'
            });
        };


        /******************************************************************************************
         * opendialog function allows to open a modal when the users click on "request query" button 
         *******************************************************************************************/
        $scope.opendialog = function () {
            return $scope.modalInstance = $uibModal.open({
                templateUrl: 'views/modalRequest.html',
                size: 'lg',
                scope: $scope,
                windowClass: 'center-modal'
            });
        };

        /***************************************
         * cancel function allows to cancel modal
         ****************************************/
        $scope.cancel = function () {
            $scope.modalInstance.dismiss("cancel");
        };

        /*******************************
         * messageinfogrid alert message
         ********************************/
        $scope.messageinfogrid = {
            textAlert: "You have to select the files in the grid before to download",
            mode: 'info'
        };
        /*******************************************************
         * messageinfonodata variable to advanced error message
         ********************************************************/
        $scope.messageinfonodata = {
            textAlert: "No data available",
            mode: 'info'
        };

        /*******************************************************************
         * openmessage function allows to open a modal when there's an error
         *******************************************************************/
        $scope.openmessage = function (message) {
            return $scope.modalInstance = $uibModal.open({
                templateUrl: 'views/messageBox.html',
                scope: $scope,
                size: 'lg',
                resolve: {
                    data: function () {
                        $scope.messageinfo = message;
                        return $scope.messageinfo;
                    }
                }
            });
        };
        /**************************************
         * close function allows to close modal
         ***************************************/
        $scope.close = function () {
            $scope.modalInstance.close();
        };

    });
