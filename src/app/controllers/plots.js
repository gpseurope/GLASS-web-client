'use strict';

/**
 * copyright (2022) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Guillaume Verbiese <guillaume.verbiese@oca.eu>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:PlotsCtrl
 * @description
 * # PlotsCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
    .controller('PlotsCtrl', function ($scope, $http, serviceResource, $uibModal, tourConfig) {

        tourConfig.animation = false;
        $scope.loading = true;
        
        $scope.loadResults = function () {

            $scope.selected =true;
            $scope.dgw = true;
             
            $scope.annualDate = $scope.dateFormat(0);
            $scope.monthlyDate = $scope.dateFormat(1);
            $scope.sixMonthDate = $scope.dateFormat(6);
            $scope.data = [{"title": "Evolution of number of stations", "link": "_Evolution_of_number_of_stations.png"},
             {"title": "Evolution of number of files", "link": "_Evolution_of_number_of_files.png"},
             {"title": "Number of stations per node", "link": "_Number_of_stations_per_node.png"},
             {"title": "Number of files per node", "link": "_Number_of_files_per_node.png"}
            ];


            $scope.generatePlots("2020-03", $scope.monthlyDate, "Annual plots", $scope.data);
            $scope.generatePlots($scope.sixMonthDate, $scope.monthlyDate, "Six last months plots", $scope.data);

        };

        /*************************************************************
		 * generatePlots function handle the display of plots on the page 
		 **************************************************************/
        $scope.generatePlots = function (lowerDate, upperDate, plotsContainerTitle, data) {
            const container = document.getElementById("container");
            const title = document.createElement("h1");
            title.classList.add("plotsResults--title");
            title.innerText = plotsContainerTitle;
            const plotsContainer = document.createElement("div");
            plotsContainer.classList.add("plotsResults");
            plotsContainer.appendChild(title);
            container.appendChild(plotsContainer);

            for (let i = 0; i < data.length; i++ ) {
                let imageContainer = document.createElement("div");
                imageContainer.classList.add("plot");
                let imageTitle = document.createElement("h3");
                imageTitle.classList.add("plot--title");
                imageTitle.innerText = data[i].title;
                let imagePlot = document.createElement("img");
                imagePlot.classList.add("plot--img");
                imagePlot.setAttribute("alt", data[i].title);
                imagePlot.setAttribute("title", data[i].title);
                imagePlot.setAttribute("src", "graphs/" + lowerDate + "_" + upperDate + data[i].link);
                imageContainer.appendChild(imagePlot);
                imageContainer.appendChild(imageTitle);
                plotsContainer.appendChild(imageContainer);
        
            }

        };

        /*************************************************************
		 * dateFormat function allowing to have a given date in a given format
		 **************************************************************/

         $scope.dateFormat = function (i) {
            var dates = new Date();
            dates.setMonth(dates.getMonth() -i);
            var mm = dates.getMonth() + 1;
		 	var yyyy = dates.getFullYear();
		 	if (mm < 10) {
				mm = '0' + mm
		 	}
		 	return dates = yyyy + '-' + mm;
         };


         $scope.loadResults();
    });