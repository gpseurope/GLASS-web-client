'use strict';

const { default: GestureHandling } = require("leaflet-gesture-handling");
const { circleMarker, popup, marker } = require("leaflet");
const { fsum } = require("d3");
const { forEach } = require("jszip");

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * 	 Arthur	Fontaine <afontaine@geoazur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:SiteCtrl
 * @description
 * # SiteCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
	.controller('SiteCtrl', function ($scope, $cookies, serviceHistory, serviceResource, $uibModal, uiGridConstants, $window, $compile, $resource, $location, uiGridExporterService, uiGridExporterConstants, $templateCache, tourConfig) {

		/***** AngularJS Tour *****/
		tourConfig.animation = false;



		/****************************************************************
		 * loadResults function allows to load the short json to the grid 
		 * and to place all markers in the map
		 ****************************************************************/
		$scope.loadResults = function () {
			//init $scope
			$scope.selected = true; //flag for advanced search button
			//values of filters in the grid
			$scope.markerselected = undefined;
			$scope.sitenameselected = undefined;
			$scope.latminselected = undefined;
			$scope.latmaxselected = undefined;
			$scope.lonminselected = undefined;
			$scope.lonmaxselected = undefined;
			$scope.altminselected = undefined;
			$scope.altmaxselected = undefined;
			$scope.networkselected = undefined;
			$scope.inversenetworkselected = undefined;
			$scope.fromdateminselected = undefined;
			$scope.fromdatemaxselected = undefined;
			$scope.todateminselected = undefined;
			$scope.todatemaxselected = undefined;
			$scope.agencyselected = undefined;
			$scope.countryselected = undefined;
			$scope.stateselected = undefined;
			$scope.cityselected = undefined;
			$scope.filterrowflag = false; //flag allow to know if we select or unselect station from the grid or the map 

			//values of the forms in the advanced search tab
			$scope.receivertypeselected = '';
			$scope.antennatypeselected = '';
			$scope.radometypeselected = '';
			$scope.satellitesystemselected = '';
			$scope.filefromdateselected = '';
			$scope.filetodateselected = '';
			$scope.publishfromdateselected = '';
			$scope.publishtodateselected = '';
			$scope.dataavailabilityselected = '';
			$scope.stationlifetimeselected = '';
			$scope.datasamplingselected = '';
			$scope.filetypeselected = '';
			$scope.statusfileselected = '';
			$scope.dataqualityselected = '';
			$scope.satellitesystemt3selected = '';
			$scope.filelatencyselected = '';
			$scope.typeofsignalselected = '';
			$scope.frequencyselected = '';
			$scope.channelselected = '';
			$scope.epochselected = '';
			$scope.elevangleselected = '';
			$scope.multipathselected = '';
			$scope.cycleslipsselected = '';
			$scope.ssprmsselected = '';
			$scope.clockjumpselected = '';
			$scope.activePanel = 0; // which panel from the advanced tab is selected

			$scope.networkStr = {}; // array of network and color
			$scope.NtwkNotSelected = []; //List of unselected networks
			$scope.simpleoradvanced = 'simple'; //flag for advanced search or simple search

			//values of the forms in the geographic selection panel
			$scope.Radius = '';
			$scope.latitude = '';
			$scope.longitude = '';
			$scope.latLong = '';
			$scope.North = '';
			$scope.Sud = '';
			$scope.West = '';
			$scope.East = '';
			$scope.slider = '';

			//values of the selection tools in the map (leaflet draw)
			$scope.layer = '';
			$scope.drawcircle = '';
			$scope.drawrectangle = '';
			$scope.drawpolygon = '';
			$scope.polygonpoints = []; // store selection polygon coordinates
			$scope.layernetworksdict = {}; // dictionnary to manage the network dropbox in the map. key: network, value: id des markers?
			$scope.resultnetworksdict = {}; // dictionnary to manage the networks in the popup. key: network, value:json

			$scope.markerselecteddict = {}; //store the selected markers. Associated to $scope.layerid. key: marker, value: marker id on the map
			$scope.layerid = ''; // store the marker id from the map. Associated to $scope.markerselecteddict	
			$scope.onlyonelayerid = false; // flag to store is only one marker or several are selected 

			$scope.networkentity = ''; //Memorize the original color of the marker before selection
			$scope.results = ''; // json from the GLASS-API request
			$scope.loading = true; // flag for the spinner

			$scope.networksdict = {}; // dictionnary of networks from the json

			$scope.firstload = true; // flag to check if it's the first load

			$scope.json = {};
			$scope.markerOnMap = []; //List of all the marker name on the map

			//arrays made from different type of networks
			$scope.local = {};
			$scope.international = {};
			$scope.virtual = {};

			//load the map
			//$scope.mymap = L.map('mapid').setView([51.505, -0.09], 3);
			$scope.mymap = L.map('mapid', {
				center: [51.505, -0.09],
				zoom: 4,
				zoomDelta: 0.5,
				minZoom: 3,
				// gestureHandling: true,
			});
			L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);

			//Scroll  with CTRL+Mouse method
			$scope.mymap.scrollWheelZoom.disable();
			$('#mapid').bind('mousewheel DOMMouseScroll', function (event) {
				event.stopPropagation();
				if (event.ctrlKey == true) {
					event.preventDefault();
					$scope.mymap.scrollWheelZoom.enable();
					setTimeout(function () {
						$scope.mymap.scrollWheelZoom.disable();
					}, 2000);
				} else {
					$scope.mymap.scrollWheelZoom.disable();
				}
			});
			//call gridHandler function to load the grid
			$scope.gridHandler();
			//first query to load all stations
			var query = serviceResource.site().query();
			$scope.loadingdialog();
			query.$promise.then(function (data) {
				$scope.ajaxresults = data; //allow to store the original json to be reused if needed
				$scope.results = data; // store the json to be processed	
				if ($scope.results !== '') {
					$scope.currentdate = $scope.currentDateFormat();
					for (var i = 0; i < $scope.ajaxresults.length; i++) {
						if ($scope.ajaxresults[i]['date_to'] === null) {
							$scope.ajaxresults[i]['date_to'] = $scope.currentdate;
						}
					}
					//create a network.json
					$scope.createNetworkJSON($scope.results);
					//call all function regarding networks
					$scope.fillNetworkStr($scope.results);
					$scope.createNetworkControl();
					//call markersHandler function to draw markers in the map with their caracteristics
					$scope.markersHandler($scope.results);
					//call a function to handle polygon,circle and rectangle button
					$scope.drawHandler();
					$scope.addEraseDraw();
					$scope.switchmap();
					//add the zoom message
					$scope.showZoomMessage();
					//initialize the grid data with what's we get from ajax response                
					$scope.gridOptions.data = $scope.results;
					$scope.gridApi.core.refresh(); //update the grid
					$scope.modalInstance.opened.then(function () {
						$scope.loading = false;
						$scope.close(); //stop the spinner loading
					});

				}
				// var cookies = $cookies.getAll();
				// var display = false
				// var display = cookies.hasOwnProperty('TeamReduced');
				// var alreadyDisplayed = sessionStorage.getItem("teamReducedModalDisplay", true);
				// if (display == false && alreadyDisplayed == null) {
				// 	$scope.teamReducedModal();
				// }

			});
			//create a watcher for radius of circle to follow the changement
			$scope.$watch("Radius", function () {
				$scope.changeRadiusCircle();
			});

		};

		/*************************************************************
		 * loadingdialog function allows to open a modal with a spinner 
		 **************************************************************/
		$scope.loadingdialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalLoading.html',
				size: 'm',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/*************************************************************************
		 * createNetworkControl function handles networks drop down list on the map
		 **************************************************************************/
		$scope.createNetworkControl = function () {
			var MyControl = L.Control.extend({
				options: {
					position: 'topright'
				},
				//style="height: 330px; width: 250px;
				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'my-custom-control');
					container.innerHTML += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 150%; margin-left:-40%">\<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Networks <span class="caret"></span></button>\
						<ul class="dropdown-menu">\
							<div class="dropdown-column">\
								<li style="font-weight:bold">Tools</li>\
									<li><input type="checkbox" value="select-all" id="select-all" checked ="checked"/> Select/Deselect All</li>\
									<li> <img src="images/black.png" height="15" width="15"> Close Panel <br /></li>\
							</div>\
							<div class="dropdown-column">\
								<li style="font-weight:bold">Virtual</li>\
								<li ng-repeat="net in virtual | orderBy:\'\' ">\
									<input type="checkbox" value="{{net}}" id="{{ net|removeSpaces }}" checked ="checked" ng-click="networkFilter()">\
									<div class="circleBorder" ng-style="{\'border-color\':\'{{colorNetworks[net].color}}\'}"></div>{{net}}<br/>\
								</li>\
							</div>\
							<div class="dropdown-column">\
								<li style="font-weight:bold">International</li>\
								<li ng-repeat="net in international | orderBy:\'\' ">\
									<input type="checkbox" value="{{net}}" id="{{ net|removeSpaces }}" checked ="checked" ng-click="networkFilter()">\
									<div class="circleBorder" ng-style="{\'border-color\':\'{{colorNetworks[net].color}}\'}"></div>{{net}}<br/>\
								</li>\
							</div>\
							<div class="dropdown-column">\
								<li style="font-weight:bold">Local</li>\
								<li ng-repeat="net in local | orderBy:\'\' ">\
									<input type="checkbox" value="{{net}}" id="{{ net|removeSpaces }}" checked ="checked" ng-click="networkFilter()">\
									<div class="circle" ng-style="{\'background-color\':\'{{colorNetworks[net].color}}\'}"></div>{{net}}<br/>\
								</li>\
							</div>\
						</ul>';
					$compile(container)($scope); // needed to compile html to template because 'ng-*' functions in the html
					return container;
				}
			});
			$scope.mymap.addControl(new MyControl());
			$('.my-custom-control #select-all').click(function (event) {
				if (this.checked) {
					$('.my-custom-control .dropdown-menu li :checkbox').each(function () {
						this.checked = true;
					});
					$scope.networkFilter();
				} else {
					$('.my-custom-control .dropdown-menu li :checkbox').each(function () {
						this.checked = false;
					});
					$scope.networkFilter();
				}
			});
		};

		/******************************************************************
		 * loadAdvanced function allows to get the response from server and 
		 * to fill the content in the listboxes  
		 ****************************************************************/
		$scope.loadAdvanced = function () {
			serviceResource.receiver().query(function (data) {
				$scope.receiver_types = data;
			})
			serviceResource.antenna().query(function (data) {
				$scope.antenna_types = data;
			})
			serviceResource.radome().query(function (data) {
				$scope.radome_types = data;
			})
			serviceResource.filetype().query(function (data) {
				$scope.filetype_result = data;
				$scope.filetype = [];
				angular.forEach($scope.filetype_result, function (item) {
					if (item.sampling_window === "24h" && item.sampling_frequency === "30s") {
						$scope.filetype.push({
							"name": item.format + " - " + item.sampling_window + " - " + item.sampling_frequency
						});
					}
				});
			})
			$scope.satellitesystem = [{
				"name": "GPS"
			}, {
				"name": "GLONASS"
			}, {
				"name": "GALILEO"
			}, {
				"name": "BEIDOU"
			}, {
				"name": "SBAS"
			}, {
				"name": "IRNSS"
			}, {
				"name": "QZSS"
			}];
			$scope.satellitesystemt3 = [{
				"name": "GPS"
			}, {
				"name": "GLONASS"
			}, {
				"name": "GALILEO"
			}, {
				"name": "BEIDOU"
			}, {
				"name": "SBAS"
			}, {
				"name": "IRNSS"
			}, {
				"name": "QZSS"
			}];
			$scope.typeofsignal = [{
				"name": "P/C"
			}, {
				"name": "L"
			}, {
				"name": "D"
			}, {
				"name": "S"
			}];
			$scope.frequencies = [{
				"name": "1"
			}, {
				"name": "2"
			}, {
				"name": "5"
			}, {
				"name": "6"
			}, {
				"name": "7"
			}, {
				"name": "8"
			}, {
				"name": "9"
			}];
			$scope.channel = [{
				"name": "No Attr"
			}, {
				"name": "P"
			}, {
				"name": "C"
			}, {
				"name": "D"
			}, {
				"name": "Y"
			}, {
				"name": "M"
			}, {
				"name": "N"
			}, {
				"name": "A"
			}, {
				"name": "B"
			}, {
				"name": "C"
			}, {
				"name": "I"
			}, {
				"name": "Q"
			}, {
				"name": "S"
			}, {
				"name": "L"
			}, {
				"name": "X"
			}, {
				"name": "W"
			}, {
				"name": "Z"
			}];
			if ($scope.isDgw()) {
				$scope.statusfile = [{
					"name": "1"
				}, {
					"name": "2"
				}, {
					"name": "3"
				}];
				$scope.statustitle = '1 = OK;  2 = Small inconsistency in rinex header;  3 = QC metrics not optimal wrt expected';
			} else {
				$scope.statusfile = [{
					"name": "-3"
				}, {
					"name": "-2"
				}, {
					"name": "-1"
				}, {
					"name": "0"
				}, {
					"name": "1"
				}, {
					"name": "2"
				}, {
					"name": "3"
				}];
				$scope.statustitle = '-3=T3 metadata incompliant with T1 metadata (alternatively not populated to DB!);-2=QC running not successful;-1=QC running;0=File not checked;1=File checked – OK according to the T1/T2/T3 metadata;2=File checked – small inconsistency in RINEX header;3=File checked – warning flag based on hardwired QC metrics (optional?)';
			}
		};

		/**********************************************************************************************
		 * changedReferenceFromDate function allows to know what's users selected in "file date install"  
		 **********************************************************************************************/
		$scope.changedReferenceFromDate = function (item) {
			if (item == undefined) {
				$scope.filefromdateselected = undefined;
			} else {
				$scope.filefromdateselected = $scope.convertDateFormat(item);
			}
		};
		/*******************************************************************************************
		 * changedReferenceToDate function allows to know what's users selected in "file date remove"  
		 *******************************************************************************************/
		$scope.changedReferenceToDate = function (item) {
			if (item == undefined) {
				$scope.filetodateselected = undefined;
			} else {
				$scope.filetodateselected = $scope.convertDateFormat(item);
			}
		};


		/*********************************************************************
		 * routeMetadatalink function handles on click links to access metadata
		 **********************************************************************/
		$scope.routeMetadatalink = function (code) {
			var param = "marker=";
			//open the view metadata with the parameter code
			var mypass = $window.open('#/metadata/' + param + code, '_blank'); //'_blank' open on another page		
		};

		/*******************************************************************************
		 * routeMetadataOrFiles function allows to access metadata or to access the files
		 *******************************************************************************/
		$scope.routeMetadataOrFiles = function (MetadataOrFiles) {
			//1)Get all the criteria enter by the user
			var params = '';
			var selectedRows = $scope.gridApi.selection.getSelectedRows();
			if (selectedRows.length >= 1) { //case row selected
				var codes = '';
				angular.forEach(selectedRows, function (row) {
					codes = codes + row.marker_long_name + ",";
				});
				codes = codes.substring(0, codes.length - 1);
				var param = "marker=";
				params = params + "&" + param + codes;
				//$scope.nodata = false;
			} else if (($scope.markerselected !== undefined) && ($scope.markerselected !== null)) { //case filter 4 char id code
				if ($scope.markerselected.length > 0) {
					var param = "marker=";
					params = params + "&" + param + $scope.markerselected;
				}
			}
			if ((selectedRows.length >= 1) || ($scope.markerselected !== undefined) || ($scope.sitenameselected !== undefined) || ($scope.latminselected !== undefined) || ($scope.latmaxselected !== undefined) || ($scope.lonminselected !== undefined) || ($scope.lonmaxselected !== undefined) || ($scope.altminselected !== undefined) || ($scope.altmaxselected !== undefined) || ($scope.networkselected !== undefined) || ($scope.inversenetworkselected !== undefined) || ($scope.fromdateminselected !== undefined) || ($scope.fromdatemaxselected !== undefined) || ($scope.todateminselected !== undefined) || ($scope.todatemaxselected !== undefined) || ($scope.agencyselected !== undefined) || ($scope.countryselected !== undefined) || ($scope.stateselected !== undefined) || ($scope.cityselected !== undefined) || ($scope.receivertypeselected !== '') || ($scope.antennatypeselected !== '') || ($scope.radometypeselected !== '') || ($scope.satellitesystemselected !== '') || ($scope.drawcircle === 'draw') || ($scope.drawrectangle === 'draw') || ($scope.drawpolygon === 'draw') || ($scope.filefromdateselected !== undefined) || ($scope.filetodateselected !== undefined) || ($scope.dataavailabilityselected !== undefined) || ($scope.stationlifetimeselected !== undefined) || ($scope.filetypeselected !== undefined) || ($scope.datasamplingselected !== undefined) || ($scope.statusfileselected !== undefined) || ($scope.typeofsignalselected !== undefined) || ($scope.frequencyselected !== undefined) || ($scope.channelselected !== undefined) || ($scope.satellitesystemt3selected !== undefined) || ($scope.epochselected !== undefined) || ($scope.elevangleselected !== undefined) || ($scope.multipathselected !== undefined) || ($scope.cycleslipsselected !== undefined) || ($scope.ssprmsselected !== undefined) || ($scope.clockjumpselected !== undefined)) {
				if ($scope.drawcircle === 'draw') {
					if (($scope.latitude !== '') && ($scope.longitude !== '') && ($scope.Radius !== '')) {
						var param = "coordinates=circle";
						params = params + "&" + param + "&centerLat=" + $scope.latitude + "&centerLon=" + $scope.longitude + "&radius=" + $scope.Radius;
					}
				}
				if ($scope.drawrectangle === 'draw') {
					if (($scope.South !== '') && ($scope.North !== '') && ($scope.West !== '') && ($scope.East !== '')) {
						var param = "coordinates=rectangle";
						params = params + "&" + param + "&minLat=" + $scope.South + "&maxLat=" + $scope.North + "&minLon=" + $scope.West + "&maxLon=" + $scope.East;
					}
				}
				if ($scope.drawpolygon === 'draw') {
					if ($scope.polygonpoints.length > 0) {
						var param = "coordinates=polygon";
						var polygoncoord = '';
						var i = 0;
						while (i < $scope.polygonpoints.length - 1) {
							if (MetadataOrFiles === "Metadata") {
								polygoncoord = polygoncoord + $scope.polygonpoints[i] + "," + $scope.polygonpoints[i + 1] + ";";
							} else if (MetadataOrFiles === "Files") {
								polygoncoord = polygoncoord + $scope.polygonpoints[i] + "!" + $scope.polygonpoints[i + 1] + ",";
							}
							i = i + 2;
						}
						polygoncoord = polygoncoord.substring(0, polygoncoord.length - 1);
						params = params + "&" + param + "&polygon=" + polygoncoord;
					}
				}
				if (($scope.sitenameselected !== undefined) && ($scope.sitenameselected !== null)) {
					if ($scope.sitenameselected.length > 0) { //check if there's something in the grid because the grid keeps in memory null value if we deleted the value in filter 
						var param = "site=";
						params = params + "&" + param + $scope.sitenameselected;
					}
				}
				if ((($scope.latminselected !== undefined) && ($scope.latminselected !== null)) || (($scope.latmaxselected !== undefined) && ($scope.latmaxselected !== null))) {
					var latitudes = '';
					if (($scope.latminselected !== undefined) && ($scope.latminselected !== null)) {
						if ($scope.latminselected.length > 0) {
							latitudes = $scope.latminselected;
						}
					}
					if (($scope.latmaxselected !== undefined) && ($scope.latmaxselected !== null)) {
						if ($scope.latmaxselected.length > 0) {
							latitudes = latitudes + "," + $scope.latmaxselected;
						}
					}
					if (latitudes != '') {
						var param = "latitude=";
						params = params + "&" + param + latitudes;
					}
				}

				if ((($scope.lonminselected !== undefined) && ($scope.lonminselected !== null)) || (($scope.lonmaxselected !== undefined) && ($scope.lonmaxselected !== null))) {
					var longitudes = '';
					if (($scope.lonminselected !== undefined) && ($scope.lonminselected !== null)) {
						if ($scope.lonminselected.length > 0) {
							longitudes = $scope.lonminselected;
						}
					}
					if (($scope.lonmaxselected !== undefined) && ($scope.lonmaxselected !== null)) {
						if ($scope.lonmaxselected.length > 0) {
							longitudes = longitudes + "," + $scope.lonmaxselected;
						}
					}
					if (longitudes != '') {
						var param = "longitude=";
						params = params + "&" + param + longitudes;
					}
				}

				if ((($scope.altminselected !== undefined) && ($scope.altminselected != null)) || (($scope.altmaxselected !== undefined) && ($scope.altmaxselected != null))) {
					var altitudes = '';
					if (($scope.altminselected !== undefined) && ($scope.altminselected != null)) {
						if ($scope.altminselected.length > 0) {
							altitudes = $scope.altminselected;
						}
					}
					if (($scope.altmaxselected !== undefined) && ($scope.altmaxselected != null)) {
						if ($scope.altmaxselected.length > 0) {
							altitudes = altitudes + "," + $scope.altmaxselected;
						}
					}
					if (altitudes != '') {
						var param = "altitude=";
						params = params + "&" + param + altitudes;
					}
				}

				if (($scope.networkselected !== undefined) && ($scope.networkselected != null)) {
					if ($scope.networkselected.length > 0) {
						var param = "network=";
						params = params + "&" + param + $scope.networkselected;
					}
				}
				if (($scope.inversenetworkselected !== undefined) && ($scope.inversenetworkselected != null)) {
					if ($scope.inversenetworkselected.length > 0) {
						var param = "invertedNetworks=";
						params = params + "&" + param + $scope.inversenetworkselected;
					}
				}
				if (($scope.fromdateminselected !== undefined) && ($scope.fromdateminselected != null)) {
					var param = "installedDateMin=";
					params = params + "&" + param + $scope.fromdateminselected;
				}
				if (($scope.fromdatemaxselected !== undefined) && ($scope.fromdatemaxselected != null)) {
					var param = "installedDateMax=";
					params = params + "&" + param + $scope.fromdatemaxselected;
				}

				if (($scope.todateminselected !== undefined) && ($scope.todateminselected != null)) {
					var param = "removedDateMin=";
					params = params + "&" + param + $scope.todateminselected;
				}
				if (($scope.todatemaxselected !== undefined) && ($scope.todatemaxselected != null)) {
					var param = "removedDateMax=";
					params = params + "&" + param + $scope.todatemaxselected;
				}
				if (($scope.agencyselected !== undefined) && ($scope.agencyselected != null)) {
					if ($scope.agencyselected.length > 0) {
						var param = "agency=";
						params = params + "&" + param + $scope.agencyselected;
					}
				}
				if (($scope.countryselected !== undefined) && ($scope.countryselected != null)) {
					if ($scope.countryselected.length > 0) {
						var param = "country=";
						params = params + "&" + param + $scope.countryselected;
					}
				}
				if (($scope.stateselected !== undefined) && ($scope.stateselected != null)) {
					if ($scope.stateselected.length > 0) {
						var param = "state=";
						params = params + "&" + param + $scope.stateselected;
					}
				}
				if (($scope.cityselected !== undefined) && ($scope.cityselected != null)) {
					if ($scope.cityselected.length > 0) {
						var param = "city=";
						params = params + "&" + param + $scope.cityselected;
					}
				}
				if ($scope.receivertypeselected.length >= 1) {
					var receiver = '';
					angular.forEach($scope.receivertypeselected, function (item) {
						receiver = receiver + item.name + ",";
					});
					receiver = receiver.substring(0, receiver.length - 1);
					var param = "receiver=";
					params = params + "&" + param + receiver;
				}
				if ($scope.antennatypeselected.length >= 1) {
					var antenna = '';
					angular.forEach($scope.antennatypeselected, function (item) {
						antenna = antenna + item.name + ",";
					});
					antenna = antenna.substring(0, antenna.length - 1);
					var param = "antenna=";
					params = params + "&" + param + antenna;
				}
				if ($scope.radometypeselected.length >= 1) {
					var radome = '';
					angular.forEach($scope.radometypeselected, function (item) {
						radome = radome + item.name + ",";
					});
					radome = radome.substring(0, radome.length - 1);
					var param = "radome=";
					params = params + "&" + param + radome;
				}
				if ($scope.satellitesystemselected.length >= 1) {
					var satellite = '';
					angular.forEach($scope.satellitesystemselected, function (item) {
						satellite = satellite + item.name.substr(0, 3) + ",";
					});
					satellite = satellite.substring(0, satellite.length - 1);
					var param = "satelliteSystem=";
					params = params + "&" + param + satellite;
				}
				if (($scope.filefromdateselected !== undefined) || ($scope.filetodateselected !== undefined)) {
					var filedate = '';
					if ($scope.filefromdateselected !== undefined) {
						if ($scope.filefromdateselected.length >= 1) {
							filedate = $scope.filefromdateselected + ",";
						}

					}
					if ($scope.filetodateselected !== undefined) {
						if ($scope.filetodateselected.length >= 1) {
							if (filedate.substring(filedate.length - 1) == ",") {
								filedate = filedate + $scope.filetodateselected;
							} else {
								filedate = filedate + "," + $scope.filetodateselected;
							}
						}

					}
					if (filedate != '') {
						// the station endpoint does not accept the comma at the end of the date but the file endpoint requires it
						if ((MetadataOrFiles === "Metadata") && (filedate.endsWith(","))) {
							filedate = filedate.substring(0, filedate.length - 1);
						}


						var param = "dateRange=";
						params = params + "&" + param + filedate;
					}
				}
				if ($scope.dataavailabilityselected.length >= 1) {
					var param = "dataAvailability=";
					params = params + "&" + param + $scope.dataavailabilityselected;
				}
				if ($scope.stationlifetimeselected.length >= 1) {
					var param = "minimumObservationYears=";
					params = params + "&" + param + $scope.stationlifetimeselected;
				}
				if ($scope.filetypeselected.length >= 1) {
					var filetype = '';
					var sampling_window = '';
					var sampling_frequecy = '';
					angular.forEach($scope.filetypeselected, function (item) {
						var item_split = item.name.split(" - ");
						filetype = filetype + item_split[0] + ",";
						sampling_window = sampling_window + item_split[1] + ",";
						sampling_frequecy = sampling_frequecy + item_split[2] + ",";
					});
					filetype = filetype.substring(0, filetype.length - 1);
					sampling_window = sampling_window.substring(0, sampling_window.length - 1);
					sampling_frequecy = sampling_frequecy.substring(0, sampling_frequecy.length - 1);
					var param1 = "fileType=";
					var param2 = "samplingWindow=";
					var param3 = "samplingFrequency=";
					params = params + "&" + param1 + filetype + "&" + param2 + sampling_window + "&" + param3 + sampling_frequecy;
				}
				if ($scope.statusfileselected.length >= 1) {
					var statusfiles = '';
					angular.forEach($scope.statusfileselected, function (item) {
						statusfiles = statusfiles + item.name + ",";
					});
					statusfiles = statusfiles.substring(0, statusfiles.length - 1);
					var param = "statusfile=";
					params = params + "&" + param + statusfiles;
				}
				if ($scope.typeofsignalselected.length >= 1) {
					var typofsignal = '';
					angular.forEach($scope.typeofsignalselected, function (item) {
						typofsignal = typofsignal + item.name + ",";
					});
					typofsignal = typofsignal.substring(0, typofsignal.length - 1);

					var param = "observationtype=";
					params = params + "&" + param + typofsignal;
				}
				if ($scope.frequencyselected.length >= 1) {
					var frequency = '';
					angular.forEach($scope.frequencyselected, function (item) {
						frequency = frequency + item.name + ",";
					});
					frequency = frequency.substring(0, frequency.length - 1);

					var param = "frequency=";
					params = params + "&" + param + frequency;
				}
				if ($scope.channelselected.length >= 1) {
					var channel = '';
					angular.forEach($scope.channelselected, function (item) {
						channel = channel + item.name + ",";
					});
					channel = channel.substring(0, channel.length - 1);

					var param = "channel=";
					params = params + "&" + param + channel;
				}
				if ($scope.satellitesystemt3selected.length >= 1) {
					var constellation = '';
					angular.forEach($scope.satellitesystemt3selected, function (item) {
						constellation = constellation + item.name + ",";
					});
					constellation = constellation.substring(0, constellation.length - 1);

					var param = "constellation=";
					params = params + "&" + param + constellation;
				}
				if ($scope.epochselected.length > 0) {
					var param = "ratioepoch=";
					params = params + "&" + param + $scope.epochselected;
				}
				if ($scope.elevangleselected.length > 0) {
					var param = "elevangle=";
					params = params + "&" + param + $scope.elevangleselected;
				}
				if ($scope.multipathselected.length > 0) {
					var param = "multipathvalue=";
					params = params + "&" + param + $scope.multipathselected;
				}
				if ($scope.cycleslipsselected.length > 0) {
					var param = "nbcycleslips=";
					params = params + "&" + param + $scope.cycleslipsselected;
				}
				if ($scope.ssprmsselected.length > 0) {
					var param = "spprms=";
					params = params + "&" + param + $scope.ssprmsselected;
				}
				if ($scope.clockjumpselected.length > 0) {
					var param = "nbclockjumps=";
					params = params + "&" + param + $scope.clockjumpselected;
				}
				if (params !== '') {
					//2)open the view with criteria enter
					params = params.substr(1);
					if (MetadataOrFiles === "Metadata") {
						var mypass = $window.open('#/metadata/' + params, '_blank');
					} else if (MetadataOrFiles === "Files") {
						var mypass = $window.open('#/file/' + params, '_blank');
					}

				} else {
					$scope.openmessage($scope.messageinfogrid);
				}
			} else {
				$scope.openmessage($scope.messageinfogrid);
			}


		};

		/**************************************************************
		 * findUnique function allows to return array with unique values
		 ***************************************************************/
		$scope.findUnique = function (arr) {
			var distinct = [];

			for (var i = 0; i < arr.length; i++) {
				var str = arr[i];
				if (distinct.indexOf(str) === -1) {
					distinct.push(str);
				}
			}
			return distinct;

		};

		/********************************************************************************************************************
		 * createColorNetworksDict function allows to have a dictionary of color for each network and get an array of networks
		 ********************************************************************************************************************/
		$scope.fillNetworkStr = function (data) {
			var arrtemp = [];
			var arrColor = {};
			for (var i = 0; i < data.length; i++) {
				var tab = $scope.makeTabFromStr(data[i].network.name);
				tab.forEach(elem => {
					if (elem.includes("NFO_")) {
						elem = "NFO"
					}
					if (!(elem in arrColor)) {
						arrtemp.push(elem);
						arrColor[elem] = {
							"color": $scope.getColor(elem)
						}
					}
				})

			}

			$scope.local = $scope.getNetworkFromType("local");
			$scope.international = $scope.getNetworkFromType("international");
			$scope.virtual = $scope.getNetworkFromType("virtual");

			$scope.networkStr = arrtemp;
			$scope.colorNetworks = arrColor;
		};

		/*************************************************************************
		 * convertDateFormat function allows to convert a date in format yyyy-mm-dd
		 **************************************************************************/
		$scope.convertDateFormat = function (date) {
			var mydate = new Date(date);
			var dd = mydate.getDate();
			var mm = mydate.getMonth() + 1;
			var yyyy = mydate.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}

			return mydate = yyyy + '-' + mm + '-' + dd;
		};

		/**************************************************************************************
		 * currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
		 **************************************************************************************/
		$scope.currentDateFormat = function () {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var HH = today.getHours();
			var MM = today.getMinutes();
			var SS = today.getSeconds();
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}
			if (HH < 10) {
				HH = "0" + HH;
			}
			if (MM < 10) {
				MM = "0" + MM;
			}
			if (SS < 10) {
				SS = "0" + SS;
			}
			return today = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM + ':' + SS;
		};

		/******************************************************************************************
		 * opendialog function allows to open a modal when the users click on "request query" button 
		 *******************************************************************************************/
		$scope.opendialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalRequest.html',
				size: 'lg',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/***************************************
		 * cancel function allows to cancel modal
		 ****************************************/
		$scope.cancel = function () {
			$scope.modalInstance.dismiss("cancel");
		};

		/*************************************************************
		 * teamReducedModal function allows to open a modal with a text message 
		 **************************************************************/
		// $scope.teamReducedModal = async function () {
		// 	await $scope.loading == false;
		// 	sessionStorage.setItem("teamReducedModalDisplay", true);
		// 	return $scope.modalInstance = $uibModal.open({
		// 		templateUrl: 'views/modalTeamReduced.html',
		// 		size: 'm',
		// 		scope: $scope,
		// 		windowClass: 'center-modal'
		// 	});
		// };

		/************************************************
		 * messageinfogrid variable to grid error message
		 ************************************************/
		$scope.messageinfogrid = {
			textAlert: "You have to select at least one thing in grid or in lisboxes",
			mode: 'info'
		};
		/*******************************************************
		 * messageinfonodata variable to advanced error message
		 ********************************************************/
		$scope.messageinfonodata = {
			textAlert: "No data available",
			mode: 'info'
		};
		/**************************************************
		 * messageinfomissing variable to grid error message
		 **************************************************/
		$scope.messageinfomissing = {
			textAlert: "You have to specify at least one station in the criteria",
			mode: 'info'
		};
		/*****************************************************
		 * messageinfonopossible variable to grid error message
		 ****************************************************/
		$scope.messageinfonopossible = {
			textAlert: "You could not fill both date range and publish date",
			mode: 'info'
		};
		/*******************************************************************
		 * openmessage function allows to open a modal when there's an error
		 *******************************************************************/
		$scope.openmessage = function (message) {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/messageBox.html',
				scope: $scope,
				size: 'lg',
				resolve: {
					data: function () {
						$scope.messageinfo = message;
						return $scope.messageinfo;
					}
				}
			});
		};

		/**************************************
		 * close function allows to close modal
		 ***************************************/
		$scope.close = function () {
			$scope.modalInstance.close();
		};

		//Potentially deprecated
		/******************************************************************************
		 * highlightFilteredHeader function handles grid highlight filter header style
		 *******************************************************************************/
		$scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
			if (col.filters[0].term) {
				return 'header-filtered';
			} else {
				return '';
			}
		};
		//Potentially deprecated
		/***************************************************************************
		 * downloadPdf function allows to exporter selected row in grid in format pdf
		 ***************************************************************************/
		$scope.downloadPdf = function () {
			var grid = $scope.gridApi.grid;
			var rowTypes = uiGridExporterConstants.SELECTED;
			var colTypes = uiGridExporterConstants.SELECTED;
			uiGridExporterService.pdfExport(grid, rowTypes, colTypes);
		};

		/**
		 * Remove special characters from mails to match the check box ids from network selection panel
		 * !! If you modify something here make sure you do the same in app.js : removeSpaces filter!!
		 * @param {string} networkName 
		 * @returns string
		 */
		$scope.formatNetworkName = function (networkName) {
			var res = networkName.replace(/&/g, '');
			res = res.replace(/[\s]/g, '');
			res = res.replace('.', '');
			res = res.replace('/', '');
			if (res.includes("NFO_")) {
				res = "NFO"
			}
			return res;
		}


		/*****************************************
		 * gridHandler function handles grid part
		 ******************************************/
		$scope.gridHandler = function (data) {
			//In the short json and the full json the field country is not defined same thing 
			if ($scope.simpleoradvanced === 'simple') //case short json
			{
				$scope.country = 'location.country.name';
			} else if ($scope.simpleoradvanced === 'advanced') //case full json
			{
				$scope.country = 'location.city.state.country.name';
			}
			//define all grid options and their columns
			$scope.gridOptions = {
				enableGridMenu: true,
				enableFiltering: true,
				enableSorting: true,
				enableRowSelection: true,
				enableSelectAll: true,
				multiSelect: true,
				exporterMenuPdf: false,
				exporterMenuExcel: false,
				exporterMenuCsv: true,
				exporterMenuAllData: false,
				enableColumnMoving: false,
				enableColumnResizing: false,
				gridMenuShowHideColumns: false,
				showGridFooter: true,
				//gridFooterTemplate: "<div>Total Items: {{grid.appScope.totalItems}}</div>",
				//all function regarding the ui-grid
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					//function allows to get all the marker displayed in the grid and then display them into the map
					$scope.gridApi.core.on.rowsRendered($scope, function () {
						if ($scope.filterrowflag === true) {
							$scope.filteredRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
							var resulttemp = [];
							var networktemp = [];
							for (var i = 0; i < $scope.filteredRows.length; i++) {
								resulttemp.push($scope.filteredRows[i].entity);
								networktemp.push($scope.filteredRows[i].entity.network.name);
							}
							// !!! besoin de checker les network pour empecher certain de s'afficher en fonction de $scope.unSelectedNetwork !!!
							$scope.mymap.eachLayer(function (layer) {
								if (layer.hasOwnProperty("_latlng") && layer != $scope.layer) { //test to avoid to delete tiles 
									$scope.mymap.removeLayer(layer);
								}
							});
							if (resulttemp.length > 0) {
								$scope.markersHandler(resulttemp);
							}
							if (networktemp.length > 0) {
								var networkuniquearray = $scope.findUnique(networktemp);
								//Transform into tab
								for (var i = 0; i < networkuniquearray.length; i++) {
									networkuniquearray[i] = $scope.makeTabFromStr(networkuniquearray[i]);
								}
								//Put every check to false
								$('.my-custom-control .dropdown-menu li input:checked').each(function () {
									this.checked = false;
								});
								//Put the right ones to true
								for (var key in networkuniquearray) {
									networkuniquearray[key].forEach(elem => {
										if ($scope.NtwkNotSelected.indexOf(elem) === -1) {
											$('#' + $scope.formatNetworkName(elem)).prop('checked', true);
										}
									});
								}
								if (networkuniquearray.length === Object.keys($scope.layernetworksdict).length) {
									$('#select-all').prop('checked', true);
								}
							} else {
								$('.my-custom-control .dropdown-menu li input:checked').each(function () {
									this.checked = false;
								});
							}
						}
					});
					//function allows to get what's the user writed in the filter, this content will store in the different $scope
					$scope.gridApi.core.on.filterChanged($scope, function () {
						$scope.filterrowflag = true;
						var grid = this.grid;
						if ($scope.drawrectangle == 'draw') {
							$scope.gridOptions.data = $scope.results;
							$scope.gridApi.core.refresh();
							setTimeout(function () {
								var newDataList = [];
								var bounds = L.latLngBounds($scope.layer._latlngs[0], $scope.layer._latlngs[2]);
								$scope.mymap.eachLayer(function (icon) { //loop throw the markers to check all markers inside the rectangle
									if (icon.hasOwnProperty("_latlng")) {
										var layerlatlong = icon.getLatLng();
										if (bounds.contains(layerlatlong)) {
											newDataList.push(icon.result);
										}
									}
								});
								$scope.gridOptions.data = newDataList; //new data for the grid
								$scope.filterrowflag = false;
								$scope.gridApi.core.refresh(); //allow to refresh the grid 
							}, 100);
						}
						if ($scope.drawcircle == 'draw') {
							$scope.gridOptions.data = $scope.results;
							$scope.gridApi.core.refresh();
							setTimeout(function () {
								var newDataList = [];
								$scope.mymap.eachLayer(function (icon) { //loop throw the markers to check all markers inside the radius
									if (icon.hasOwnProperty("_latlng") && icon.result !== undefined) {
										var layerlatlong = icon.getLatLng();
										var distancefromcenterPoint = layerlatlong.distanceTo($scope.latLong); //$scope.latLong is the center
										if (distancefromcenterPoint <= $scope.layer._mRadius) {
											newDataList.push(icon.result);
										}
									}
								});
								$scope.filterrowflag = false;
								$scope.gridOptions.data = newDataList; //new data for the grid
								$scope.gridApi.core.refresh(); //allow to refresh the grid 
							}, 100);
						}
						if ($scope.drawpolygon == 'draw') {
							$scope.gridOptions.data = $scope.results;
							$scope.gridApi.core.refresh();
							setTimeout(function () {
								var newDataList = [];
								for (var key in $scope.networksdict) {
									for (var i = 0; i < $scope.networksdict[key].length; i++) {
										if ($scope.isMarkerInsidePolygon($scope.networksdict[key][i], $scope.layer)) //call the function to check it
										{
											newDataList.push($scope.networksdict[key][i].result);
										}
									}
								}
								for (var i = 0; i < $scope.layer._latlngs.length; i++) {
									$scope.polygonpoints.push($scope.layer._latlngs[i].lat.toFixed(6));
									$scope.polygonpoints.push($scope.layer._latlngs[i].lng.toFixed(6));
								}
								$scope.gridOptions.data = newDataList; //new data for the grid
								$scope.filterrowflag = false;
								$scope.gridApi.core.refresh(); //allow to refresh the grid  
							}, 100);
						}
						$.each(grid.columns, function (index, column) {
							switch (column.field) {
								case 'marker_long_name':
									$scope.markerselected = column.filters[0].term;
									break;
								case 'name':
									$scope.sitenameselected = column.filters[0].term;
									break;
								case 'location.coordinates.lat':
									$scope.latminselected = column.filters[0].term;
									$scope.latmaxselected = column.filters[1].term;
									break;
								case 'location.coordinates.lon':
									$scope.lonminselected = column.filters[0].term;
									$scope.lonmaxselected = column.filters[1].term;
									break;
								case 'location.coordinates.altitude':
									$scope.altminselected = column.filters[0].term;
									$scope.altmaxselected = column.filters[1].term;
									break;
								case 'date_from':
									$scope.fromdateminselected = column.filters[0].term;
									$scope.fromdatemaxselected = column.filters[1].term;
									break;
								case 'date_to':
									$scope.todateminselected = column.filters[0].term;
									$scope.todatemaxselected = column.filters[1].term;
									break;
								case 'location.country.name':
									$scope.countryselected = column.filters[0].term;
									break;
								case 'location.state.name':
									$scope.stateselected = column.filters[0].term;
									break;
								case 'location.city.name':
									$scope.cityselected = column.filters[0].term;
									break;
								case 'agency.name':
									$scope.agencyselected = column.filters[0].term;
									break;
								case 'network.name':
									$scope.networkselected = column.filters[0].term;
									$scope.inversenetworkselected = column.filters[1].term;
									break;
							}
						});
					});
					//function allows to get the row selected in the grid and display the corresponding marker into the map with a circle bigger
					$scope.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
						if (row.entity.date_to === $scope.currentdate) {
							$scope.mymap.eachLayer(function (layer) {
								if (layer.hasOwnProperty("_latlng")) {
									if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {
										var mylayerid = layer._leaflet_id;
										if (row.isSelected) {
											var highlight = {
												radius: 12,
												fillColor: "#2CDBF6",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
											};
											$scope.mymap._layers[mylayerid].setStyle(highlight);
											$scope.mymap._layers[mylayerid].bringToFront();
											$scope.markerselecteddict[mylayerid] = row.entity.marker; //store the marker in the dictionnary to use later
										} else {
											$scope.mymap._layers[mylayerid].setStyle($scope.getOption($scope.makeTabFromStr(row.entity.network.name)));
											delete $scope.markerselecteddict[mylayerid];
										}
									}
								}
							});
						}
					});
					//function allows to get several row selected in the grid and display them into the map with a circle bigger
					$scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
						angular.forEach(rows, function (row, key) {
							if (row.entity.date_to === $scope.currentdate) {
								$scope.mymap.eachLayer(function (layer) {
									if (layer.hasOwnProperty("_latlng")) {
										if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {
											var defaultoption = $scope.getOption($scope.makeTabFromStr(row.entity.network.name));
											var mylayerid = layer._leaflet_id;
											if (row.isSelected) {
												var highlight = {
													radius: 12,
													fillColor: "#2CDBF6",
													color: "#000",
													weight: 1,
													opacity: 1,
													fillOpacity: 0.8
												};
												$scope.mymap._layers[mylayerid].setStyle(highlight);
												$scope.mymap._layers[mylayerid].bringToFront();
												$scope.markerselecteddict[mylayerid] = row.entity.marker;
											} else {
												$scope.mymap._layers[mylayerid].setStyle(defaultoption);
												delete $scope.markerselecteddict[mylayerid];
											}
										}
									}
								});
							}
						});
					});
				},
				//definition of each column in the grid
				columnDefs: [{
					name: 'Marker',
					field: 'marker_long_name',
					enableColumnMenu: false,
					checkboxSelection: true,
					headerCellClass: 'colorheader',
					sort: { direction: uiGridConstants.ASC, priority: 1 },
					filter: {
						condition: function (searchTerm, cellValue) { //function handles regular expression
							var separators = [','];
							var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
							var bReturnValue = false;
							for (var iIndex in strippedValue) {
								var sValueToTest = strippedValue[iIndex];
								if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
									bReturnValue = true;
							}
							return bReturnValue;
						}
					},
					cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.routeMetadatalink(row.entity.marker_long_name)">{{ COL_FIELD }}</a></div>'
				},
				{
					name: 'Site name',
					field: 'name',
					enableColumnMenu: false,

					headerCellClass: 'colorheader',
					filter: {
						condition: function (searchTerm, cellValue) {
							var separators = [','];
							var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
							var bReturnValue = false;
							for (var iIndex in strippedValue) {
								var sValueToTest = strippedValue[iIndex];
								if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
									bReturnValue = true;
							}
							return bReturnValue;
						}
					}
				},
				{
					name: 'Lat',
					field: 'location.coordinates.lat',
					type: 'number',
					cellFilter: 'number: 3',
					enableColumnMenu: false,

					headerCellClass: 'colorheader',
					filters: [{
						condition: uiGridConstants.filter.GREATER_THAN,
						placeholder: 'greater than'
					},
					{
						condition: uiGridConstants.filter.LESS_THAN,
						placeholder: 'less than'
					}
					]
				},
				{
					name: 'Lon',
					field: 'location.coordinates.lon',
					type: 'number',
					cellFilter: 'number: 3',
					enableColumnMenu: false,

					headerCellClass: 'colorheader',
					filters: [{
						condition: uiGridConstants.filter.GREATER_THAN,
						placeholder: 'greater than'
					},
					{
						condition: uiGridConstants.filter.LESS_THAN,
						placeholder: 'less than'
					}
					]
				},
				{
					name: 'Alt',
					field: 'location.coordinates.altitude',
					type: 'number',
					cellFilter: 'number: 3',
					enableColumnMenu: false,

					headerCellClass: 'colorheader',
					filters: [{
						condition: uiGridConstants.filter.GREATER_THAN,
						placeholder: 'greater than'
					},
					{
						condition: uiGridConstants.filter.LESS_THAN,
						placeholder: 'less than'
					}
					]
				},
				{
					name: 'Install Date',
					field: 'date_from',
					enableColumnMenu: false,

					headerCellClass: 'colorheader',
					filters: [{
						condition: function (term, value) {
							if (!term) return true;
							var valueDate = new Date(value.replace(' ', 'T'));
							console.log(valueDate);
							var replaced = term.replace(/\\/g, '');
							var termDate = new Date(replaced);
							return valueDate > termDate;
						},
						placeholder: 'greater than'
					},
					{
						condition: function (term, value) {
							if (!term) return true;
							var valueDate = new Date(value.replace(' ', 'T'));
							var replaced = term.replace(/\\/g, '');
							var termDate = new Date(replaced);
							return valueDate < termDate;
						},
						placeholder: 'less than'
					}
					]
				},
				{
					name: 'End Date',
					field: 'date_to',
					enableColumnMenu: false,
					headerCellClass: 'colorheader',
					filters: [{
						condition: function (term, value) {
							if (!term) return true;
							var valueDate = new Date(value);
							var replaced = term.replace(/\\/g, '');
							var termDate = new Date(replaced);
							return valueDate > termDate;
						},
						placeholder: 'greater than'
					},
					{
						condition: function (term, value) {
							if (!term) return true;
							var valueDate = new Date(value);
							var replaced = term.replace(/\\/g, '');
							var termDate = new Date(replaced);
							return valueDate < termDate;
						},
						placeholder: 'less than'
					}
					],
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						if (grid.getCellValue(row, col) == $scope.currentdate) {
							return 'white';
						}
					}
				},
				{
					name: 'Country',
					field: 'location.country.name',
					enableColumnMenu: false,
					headerCellClass: 'colorheader',
					filter: {
						condition: function (searchTerm, cellValue) {
							var separators = [','];
							var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
							var bReturnValue = false;
							for (var iIndex in strippedValue) {
								var sValueToTest = strippedValue[iIndex];
								if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
									bReturnValue = true;
							}
							return bReturnValue;
						}
					}
				},
				{
					name: 'State',
					field: 'location.state.name',
					enableColumnMenu: false,
					headerCellClass: 'colorheader',
					filter: {
						condition: function (searchTerm, cellValue) {
							var separators = [','];
							var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
							var bReturnValue = false;
							for (var iIndex in strippedValue) {
								var sValueToTest = strippedValue[iIndex];
								if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
									bReturnValue = true;
							}
							return bReturnValue;
						}
					}
				},
				{
					name: 'City',
					field: 'location.city.name',
					enableColumnMenu: false,
					headerCellClass: 'colorheader',
					filter: {
						condition: function (searchTerm, cellValue) {
							var separators = [','];
							var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
							var bReturnValue = false;
							for (var iIndex in strippedValue) {
								var sValueToTest = strippedValue[iIndex];
								if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
									bReturnValue = true;
							}
							return bReturnValue;
						}
					}
				},
				{
					name: 'Agency',
					field: 'agency.name',
					enableColumnMenu: false,
					headerCellClass: 'colorheader',
					filter: {
						condition: function (searchTerm, cellValue) {
							var separators = [','];
							var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
							var bReturnValue = false;
							for (var iIndex in strippedValue) {
								var sValueToTest = strippedValue[iIndex];
								if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
									bReturnValue = true;
							}
							return bReturnValue;
						}
					}
				},
				{
					name: 'Network',
					field: 'network.name',
					enableColumnMenu: false,
					headerCellClass: 'colorheader',
					filters: [{
						condition: function (searchTerm, cellValue) {
							var networkslist = $scope.makeTabFromStr(cellValue);
							var bReturnValue = false;
							networkslist.forEach(elem => {
								var name = elem;
								for (var i = 0; i < name.length; i++) {
									var separators = [','];
									var strippedValue = searchTerm.split(separators);

									for (var iIndex in strippedValue) {
										var sValueToTest = strippedValue[iIndex].replace('\\', '');
										if (name.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
											bReturnValue = true;
									}
								}
							});
							return bReturnValue;
						},
						placeholder: 'ex: epn,igs,...'
					},
					{
						condition: function (searchTerm, cellValue) {
							var networkslist = $scope.makeTabFromStr(cellValue);
							var bReturnValue = true;
							networkslist.forEach(elem => {
								var name = elem;
								for (var i = 0; i < name.length; i++) {
									var separators = [','];
									var strippedValue = searchTerm.split(separators);
									for (var iIndex in strippedValue) {
										var sValueToTest = strippedValue[iIndex];
										if (name.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
											bReturnValue = false;
									}
								}
							});
							return bReturnValue;
						},
						placeholder: 'inverse filter'
					}],
					cellTemplate: '<div class="ui-grid-cell-contents" ng-click="grid.appScope.networkPopUp(row);" style="cursor:pointer;">\
						<span id="gridlink" '/*ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[0])"*/ + '>{{ grid.appScope.splitNetworklink(row.entity.network.name)[0] }}</span>\
						<span id="gridlink" '/*ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[1])"*/ + '>{{ grid.appScope.splitNetworklink(row.entity.network.name)[1] }}</span>\
						<span id="gridlink" '/*ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[2])"*/ + '>{{ grid.appScope.splitNetworklink(row.entity.network.name)[2] }}</span>\
						<span id="gridlink" '/*ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[3])"*/ + '>{{ grid.appScope.splitNetworklink(row.entity.network.name)[3] }}</span>\
						<span id="gridlink" '/*ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[4])"*/ + '>{{ grid.appScope.splitNetworklink(row.entity.network.name)[4] }}</span>\
						<span id="gridlink" '/*ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[5])"*/ + '>{{ grid.appScope.splitNetworklink(row.entity.network.name)[5] }}</span>\
						</div>'
				}
				],
				data: data,
				rowTemplate: '<div ng-mouseover="grid.appScope.onRowHover(row);" ng-mouseleave="grid.appScope.onRowLeave(row);"><div ng-repeat="col in colContainer.renderedColumns track by col.colDef.name"  class="ui-grid-cell" ui-grid-cell></div></div>'
			};
		};
		/****************************************************************
		 * onRowHover function handles the mouse over the row of grid
		 ****************************************************************/
		$scope.onRowHover = function (row) {
			if (row.entity.date_to === $scope.currentdate) //handle only the active station
			{
				$scope.mymap.eachLayer(function (layer) {
					if (layer.hasOwnProperty("_latlng")) {
						if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {
							if (!(layer._leaflet_id in $scope.markerselecteddict)) { //avoid to change markers selected
								var highlight = {
									radius: 12,
									fillColor: "#FCFC33",
									color: "#000",
									weight: 1,
									opacity: 1,
									fillOpacity: 0.8
								};
								if ((layer._leaflet_id !== '') && ($scope.onlyonelayerid == false)) {
									$scope.onlyonelayerid = true;
									$scope.layerid = layer._leaflet_id; //keep in $scope the layer id
									$scope.networkentity = row.entity.network.name; //keep in $scope the network to allow to back to the default color
									$scope.mymap._layers[layer._leaflet_id].setStyle(highlight); //change the color of the marker
									$scope.mymap._layers[layer._leaflet_id].bringToFront();
								}
							}
						}
					}
				});
			}
		};
		/****************************************************************
		 * onRowLeave function handles the mouse leave the row of grid
		 ****************************************************************/
		$scope.onRowLeave = function (row) {
			if ((!($scope.layerid in $scope.markerselecteddict)) && ($scope.layerid != '')) { //avoid to change markers selected
				var defaultoption = $scope.getOption($scope.makeTabFromStr(row.entity.network.name));
				if ($scope.layerid != '') {
					$scope.mymap._layers[$scope.layerid].setStyle(defaultoption); //back to default color of marker
				}
				$scope.onlyonelayerid = false;
				$scope.layerid = '';
			}
		};


		/**
		 * Show networks in a pop up
		 */
		$scope.networkPopUp = function (row) {
			var popupNetwork = document.getElementById("networkList");
			var marker = document.getElementById("networkListMarker");
			var listNetwork = document.getElementById("networkListValues");
			listNetwork.innerHTML = "";
			if (marker.innerHTML == row.entity.marker_long_name) {
				marker.innerHTML = "";
				popupNetwork.style.display = "none";
			} else {
				marker.innerHTML = row.entity.marker_long_name;
				var ntwk = row.entity.network.name.split(' & ');
				for (var i = 0; i < ntwk.length; i++) {
					listNetwork.innerHTML += '<a href="#/network/' + ntwk[i] + '" target="_blank">' + ntwk[i] + ' | </a>';
				}
				popupNetwork.style.display = "block";
			}
		}
		$scope.closePopUp = function () {
			var popupNetwork = document.getElementById("networkList");
			popupNetwork.style.display = "none"
		}

		/*******************************************************************
		 * zoomInitial function allows to back to the initial view of the map
		 *******************************************************************/
		$scope.zoomInitial = function () {
			$scope.mymap.setView([51.505, -0.09], 3);
		};

		/******************************************
		 * clickClear function handles clear button
		 ******************************************/
		$scope.clickClear = function () {
			$scope.drawcircle = '';
			$scope.drawrectangle = '';
			$scope.latitude = '';
			$scope.longitude = '';
			$scope.Radius = '';
			$scope.latLong = '';
			$scope.North = '';
			$scope.South = '';
			$scope.East = '';
			$scope.West = '';
			$scope.mymap.removeLayer($scope.layer);
			$scope.layer = '';
			$scope.gridApi.core.clearAllFilters();
			$scope.receivertypeselected = '';
			$scope.antennatypeselected = '';
			$scope.radometypeselected = '';
			$scope.satellitesystemselected = '';
			$scope.filefromdateselected = '';
			$scope.filetodateselected = '';
			$scope.publishfromdateselected = '';
			$scope.publishtodateselected = '';
			$scope.dataavailabilityselected = '';
			$scope.stationlifetimeselected = '';
			$scope.datasamplingselected = '';
			$scope.filetypeselected = '';
			$scope.statusfileselected = '';
			$scope.dataqualityselected = '';
			$scope.satellitesystemt3selected = '';
			$scope.filelatencyselected = '';
			$scope.typeofsignalselected = '';
			$scope.frequencyselected = '';
			$scope.channelselected = '';
			$scope.epochselected = '';
			$scope.elevangleselected = '';
			$scope.multipathselected = '';
			$scope.cycleslipsselected = '';
			$scope.ssprmsselected = '';
			$scope.clockjumpselected = '';
			$scope.gridApi.selection.clearSelectedRows();
			$scope.markerselecteddict = {};
			$scope.polygonpoints = [];
			$scope.drawpolygon = '';
			$scope.gridOptions.data = $scope.ajaxresults;
			$scope.NtwkNotSelected = [];
			$scope.gridApi.core.refresh();
			$scope.zoomInitial();
		};

		/**
		 * Make a tab from the given string
		 * @param {String} str 
		 */
		$scope.makeTabFromStr = function (str) {
			//var str = str.replace('.','');
			return str.split(' & ');
		}

		/**
		 * Return the options needed for this station ( on the map )
		 * @param {String[]} networkNames 
		 */

		$scope.getOption = function (networkNames) {
			var options = { radius: 7, fillColor: "", color: "#000", weight: 0, opacity: 1, fillOpacity: 0.8 };
			networkNames.forEach(element => {
				var name = element;
				if (name.includes("NFO_"))
					name = "NFO";
				$scope.json.forEach(elem => {
					if (name == elem.network) {
						switch (elem.type) {
							case "local":
								options.fillColor = elem.color;
								break;
							case "international":
								if (options.fillColor === "")
									options.fillColor = "#FFFFFF";
								//if(options.color==="#000") //Put priority to virtual for the borders (change only if not already modified)
								options.color = elem.color;
								options.weight = 2;
								break;
							case "virtual":
								if (options.fillColor === "")
									options.fillColor = "#FFFFFF";
								if (options.color == "#000") // Give priority to international networks
									options.color = elem.color;
								if (name == "E-GVAP")
									options.weight = 1;
								else
									options.weight = 2;
								break;
							default:
								options = { radius: 7, fillColor: "#00FF00", color: "#000", weight: 1, opacity: 1, fillOpacity: 0.8 };
						}
					}
				});
			});
			return options;
		}
		/**
		 * Return the network's color
		 */
		$scope.getColor = function (networkStr) {
			var tab = $scope.makeTabFromStr(networkStr);
			var res = "";
			tab.forEach(element => {
				var name = element;
				if (name.includes("NFO_"))
					name = "NFO";
				$scope.json.forEach(elem => {
					if (name == elem.network) {
						if (elem.type !== "international")
							res = elem.color;
						else if (name === "IGS")
							res = elem.color;
						else
							res = elem.color;
					}
				});
			});
			return res;
		}

		/**
		 * return every network of the given type
		 * @param {String} type
		 */
		$scope.getNetworkFromType = function (type) {
			var res = [];
			$scope.json.forEach(elem => {
				if (elem.type === type && res.indexOf(elem.network) === -1) {
					res.push(elem.network);
				}
			})
			return res;
		}

		/********************************************************************
		 * markersHandler function handles the display of markers in the map
		 ********************************************************************/
		$scope.markersHandler = function (data) {
			var marker = '';
			var networkStr = '';
			var markerarray = [];
			var date_end = '';
			angular.forEach(data, function (result) {
				//if(!(result.marker_long_name in $scope.markerOnMap)){
				networkStr = result.network.name;
				//networkTab = result.network_array;
				var networkTab = $scope.makeTabFromStr(networkStr);
				//active station 
				if (result.date_to === $scope.currentdate) {
					var options = $scope.getOption(networkTab);
					marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon], options);
					date_end = "";
				} else { //desactive station
					var LeafIcon = L.Icon.extend({
						options: {
							iconSize: [15, 15],
							iconAnchor: [15, 15]
						}
					});

					// var mycolor = $scope.colorNetworks[networkStr].color;
					var mypathicon = "images/black.png";
					marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon], {
						icon: new LeafIcon({
							iconUrl: mypathicon
						})
					});
					date_end = result.date_to;
				}

				var container = $('<div />'); //create a container for popup
				marker.result = result; //allow to store in the marker all the informations

				var markerdouble = '';
				var markerdoublelongname = '';
				var datefromdouble = '';
				var datetodouble = '';
				var altitudedouble = '';

				for (var i = 0; i < data.length; i++) {
					if ((data[i].location.coordinates.lat === result.location.coordinates.lat) && (data[i].location.coordinates.lon === result.location.coordinates.lon)) {
						if (data[i].marker_long_name != result.marker_long_name) {
							markerdouble = data[i].marker_long_name;
							markerdoublelongname = data[i].marker_long_name;
							datefromdouble = data[i].date_from;
							if (data[i].date_to === $scope.currentdate) {
								datetodouble = "";
							} else {
								datetodouble = data[i].date_to;
							}
							altitudedouble = data[i].location.coordinates.altitude;
						}
					}
				}
				if (markerdouble != '') { //case two stations at a same coordinates

					container.on('click', '.popupLink', function () {
						$scope.routeMetadatalink(result.marker_long_name);
					});
					container.on('click', '.popupLink2', function () {
						$scope.routeMetadatalink(markerdouble);
					});

					container.html('<div id="popupcontainer"><div><span><b>Marker:</b> ' + result.marker_long_name +
						'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from +
						'</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end +
						'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.country.name +
						'</span><br style="line-height:0px;"><span><b>Network:</b> ' + result.network.name +
						'</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a></div>' +
						'<div><span><b>Marker:</b> ' + markerdoublelongname +
						'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Alt:</b> ' + altitudedouble.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + datefromdouble +
						'</span><br style="line-height:0px;"><span><b>End date:</b> ' + datetodouble +
						'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.country.name +
						'</span><br style="line-height:0px;"><span><b>Network:</b> ' + result.network.name +
						'</span><br style="line-height:0px;"><a class="popupLink2">Metadata details</a></div></div>'
					);
				} else {

					container.on('click', '.popupLink', function () {
						$scope.routeMetadatalink(result.marker_long_name);
					});

					container.html('<span><b>Marker:</b> ' + result.marker_long_name +
						'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from +
						'</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end +
						'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.country.name +
						'</span><br style="line-height:0px;"><span><b>Network:</b> ' + result.network.name +
						'</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a>'
					);
				}

				marker.bindPopup(container[0]); //bind the marker to the popup
				marker.on('click', function (e) {
					var markerselectedflag = false;
					for (var key in $scope.markerselecteddict) {
						if ($scope.markerselecteddict[key] === result.marker_long_name) {
							markerselectedflag = true;
						}
					}
					if (markerselectedflag === true) { //if we click on marker already selected, we unselected this marker
						$scope.latitude = '';
						$scope.longitude = '';
						angular.forEach($scope.gridOptions.data, function (data, index) {
							if (data.marker_long_name === result.marker_long_name) {
								$scope.gridApi.selection.unSelectRow($scope.gridOptions.data[index]);
							}
						});
						$scope.mymap.closePopup();
					} else { //if we click on marker not selected, we select it in the grid too
						$scope.latitude = result.location.coordinates.lat;
						$scope.longitude = result.location.coordinates.lon;
						angular.forEach($scope.gridOptions.data, function (data, index) {
							if (data.marker_long_name === result.marker_long_name) {
								$scope.gridApi.selection.selectRow($scope.gridOptions.data[index]); //select the row in the grid
							}
						});
					}
					$scope.$apply(); //allow to make an update
				});

				if ($scope.firstload === true) {
					if (networkStr in $scope.networksdict) {
						$scope.networksdict[networkStr].push(marker);
						$scope.resultnetworksdict[networkStr].push(result);
					} else {
						$scope.networksdict[networkStr] = [marker];
						$scope.resultnetworksdict[networkStr] = [result];
					}
				} else {
					markerarray.push(marker)
				}
				networkStr = '';
				$scope.markerOnMap.push(result.marker_long_name);
				//}
			});
			var refnetwork = ''
			if ($scope.firstload === true) {
				for (var key in $scope.networksdict) {
					$scope.layernetworksdict[key] = new L.FeatureGroup($scope.networksdict[key]).addTo($scope.mymap);

				}
				$scope.firstload = false;
			} else {
				refnetwork = new L.FeatureGroup(markerarray).addTo($scope.mymap);
				//$scope.mymap.fitBounds(refnetwork.getBounds(), {maxZoom: 7});
			}


		};


		/*************************************************
		 * addEraseDraw function allow to remove the draw
		 *************************************************/
		$scope.addEraseDraw = function () {
			var MyControl = L.Control.extend({
				options: {
					position: 'topleft'
				},
				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'erase-control');
					container.innerHTML += '<button style="background-image:url(images/erase.png);width:28px;height:28px;border-radius:5px;" title="Erase"></button>';
					$compile(container)($scope);
					return container;
				}
			});
			$scope.mymap.addControl(new MyControl());
			$('.erase-control').click(function (event) {
				$scope.gridOptions.data = $scope.ajaxresults;
				$scope.gridApi.core.refresh();
				$scope.drawcircle = '';
				$scope.drawrectangle = '';
				$scope.latitude = '';
				$scope.longitude = '';
				$scope.Radius = '';
				$scope.latLong = '';
				$scope.North = '';
				$scope.South = '';
				$scope.East = '';
				$scope.West = '';
				$scope.mymap.removeLayer($scope.layer);
				$scope.layer = '';
				$scope.polygonpoints = [];
				$scope.drawpolygon = '';
			});
		};

		/*******************************************************************
		 * switchmap function allow to switch between satellite or simple map
		 *******************************************************************/
		$scope.switchmap = function () {
			var MyControl = L.Control.extend({
				options: {
					position: 'bottomleft'
				},
				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'switchmap-control');
					container.innerHTML += '<button id="mapbuttonid" style="background-image:url(images/satellitemap.png);width:70px;height:70px;"></button>';
					$scope.imagemap = "satellitemap";
					$compile(container)($scope);
					return container;
				}
			});
			$scope.mymap.addControl(new MyControl());
			$('.switchmap-control').click(function (event) {
				if ($scope.imagemap == "satellitemap") {
					L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					document.getElementById("mapbuttonid").style.backgroundImage = "url(images/simplemap.png)";
					$scope.imagemap = "simplemap";
				} else {
					L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					document.getElementById("mapbuttonid").style.backgroundImage = "url(images/satellitemap.png)";
					$scope.imagemap = "satellitemap";
				}
			});
		};


		/*********************************************************************************
		 * drawHandler function handles the figure polygon, rectangle and circle in the map
		 *********************************************************************************/
		$scope.drawHandler = function () {
			var MyControl = L.Control.extend({
				options: {
					position: 'topleft'
				},
				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'figure-control');
					container.innerHTML += '<div><button id="polygon" style="background-image:url(images/polygon.png);width:28px;height:28px;border-radius:5px;" title="draw polygon"></button></div><div><button id="rectangle" style="background-image:url(images/rectangle.png);width:28px;height:28px;border-radius:5px;" title="draw rectangle"></button></div><div></button><button id="circle" style="background-image:url(images/circle.png);width:28px;height:28px;border-radius:5px;" title="draw circle"></button></div>';
					$compile(container)($scope);
					return container;
				}
			});
			$scope.mymap.addControl(new MyControl());
			$('.figure-control #polygon').click(function (event) {
				var polygon = new L.Draw.Polygon($scope.mymap, {
					allowIntersection: false,
					shapeOptions: {
						color: '#007FFF'
					}
				});
				polygon.enable();
			});
			$('.figure-control #rectangle').click(function (event) {
				var rectangle = new L.Draw.Rectangle($scope.mymap, {
					shapeOptions: {
						color: '#007FFF'
					}
				});
				rectangle.enable();
			});
			$('.figure-control #circle').click(function (event) {
				var circle = new L.Draw.Circle($scope.mymap, {
					shapeOptions: {
						color: '#007FFF'
					}
				});
				circle.enable();
			});

			$scope.mymap.on('draw:created', function (e) { //all events for each figure
				var newDataList = [];
				var type = e.layerType;
				$scope.layer = e.layer;
				if (type === 'circle') {
					$scope.drawcircle = 'draw';
					$scope.latLong = [$scope.layer._latlng.lat, $scope.layer._latlng.lng];
					var theCenterPt = $scope.latLong;
					var theRadius = $scope.layer._mRadius;
					$scope.mymap.eachLayer(function (icon) { //loop throw the markers to check all markers inside the radius
						if (icon.hasOwnProperty("_latlng")) {
							var layerlatlong = icon.getLatLng();
							var distancefromcenterPoint = layerlatlong.distanceTo(theCenterPt);
							if (distancefromcenterPoint <= theRadius) {
								newDataList.push(icon.result);
							}
						}
					});
					$scope.Radius = (theRadius / 1000).toFixed(1);
					$scope.latitude = $scope.layer._latlng.lat.toFixed(6);
					$scope.longitude = $scope.layer._latlng.lng.toFixed(6);
					$scope.gridOptions.data = newDataList; //new data for the grid
					$scope.filterrowflag = false;
					$scope.gridApi.core.refresh(); //allow to refresh the grid  
				}
				if (type === 'rectangle') {
					$scope.drawrectangle = 'draw';
					var bounds = L.latLngBounds($scope.layer._latlngs[0], $scope.layer._latlngs[2]);
					$scope.mymap.eachLayer(function (icon) { //loop throw the markers to check all markers inside the rectangle
						if (icon.hasOwnProperty("_latlng")) {
							var layerlatlong = icon.getLatLng();
							if (bounds.contains(layerlatlong)) {
								newDataList.push(icon.result);
							}
						}
					});
					$scope.South = $scope.layer._latlngs[0].lat.toFixed(6);
					$scope.North = $scope.layer._latlngs[2].lat.toFixed(6);
					$scope.West = $scope.layer._latlngs[0].lng.toFixed(6);
					$scope.East = $scope.layer._latlngs[2].lng.toFixed(6);
					var middlelat = (parseFloat($scope.South) + parseFloat($scope.North)) / 2;
					var middlelon = (parseFloat($scope.East) + parseFloat($scope.West)) / 2;
					$scope.latLong = [middlelat, middlelon];
					$scope.gridOptions.data = newDataList; //new data for the grid
					$scope.filterrowflag = false;
					$scope.gridApi.core.refresh(); //allow to refresh the grid  
				}
				if (type === 'polygon') {
					$scope.drawpolygon = 'draw';
					for (var key in $scope.networksdict) {
						for (var i = 0; i < $scope.networksdict[key].length; i++) {
							if ($scope.isMarkerInsidePolygon($scope.networksdict[key][i], $scope.layer)) //call the function to check it
							{
								newDataList.push($scope.networksdict[key][i].result);
							}
						}
					}
					for (var i = 0; i < $scope.layer._latlngs.length; i++) {
						$scope.polygonpoints.push($scope.layer._latlngs[i].lat.toFixed(6));
						$scope.polygonpoints.push($scope.layer._latlngs[i].lng.toFixed(6));
					}
					$scope.gridOptions.data = newDataList; //new data for the grid
					$scope.filterrowflag = false;
					$scope.gridApi.core.refresh(); //allow to refresh the grid  
				}
				$scope.mymap.addLayer($scope.layer);
			});
			$scope.mymap.on('draw:drawstart', function (e) { //at the beginning of drawing
				$scope.mymap.removeLayer($scope.layer);
				$scope.layer = '';
				$scope.drawcircle = '';
				$scope.drawrectangle = '';
				$scope.latitude = '';
				$scope.longitude = '';
				$scope.Radius = '';
				$scope.latLong = '';
				$scope.North = '';
				$scope.South = '';
				$scope.East = '';
				$scope.West = '';
				$scope.polygonpoints = [];
				$scope.drawpolygon = '';
				$scope.$apply(); //apply an update
			});
		};

		/********************************************************************************
		 * networkFilter function handles the selected network in "network" drop down list
		 *********************************************************************************/
		$scope.networkFilter = function () {
			var networkselectresults = [];
			var selected = [];
			var unselected = [];
			var layers = $scope.layernetworksdict;

			$('.my-custom-control .dropdown-menu li input:checked').each(function () { //take all checked networks in the dropdown list
				if ($(this).attr('value') !== "select-all")
					selected.push($(this).attr('value'));
			});
			$('.my-custom-control .dropdown-menu li input:checkbox:not(:checked)').each(function () { //take all checked networks in the dropdown list
				if ($(this).attr('value') !== "select-all")
					unselected.push($(this).attr('value'));
			});
			$scope.NtwkNotSelected = unselected;

			if (selected.length >= 1) { //case there's something selected
				console.log(selected)
				for (var key in layers) {
					var isToShow = false;
					var tab = $scope.makeTabFromStr(key);
					tab.forEach(ntwk => {
						if (ntwk.includes("NFO_")) {
							ntwk = "NFO"
						}
						// put/remove makers belonging to the selected networks in the map dropdown	
						if (selected.indexOf(ntwk) !== -1) {
							isToShow = true;
						}
					});
					if (isToShow) {
						networkselectresults = networkselectresults.concat($scope.resultnetworksdict[key]);
					}
				}
			} else { //case nothing is selected (deselectall)
				for (key in $scope.resultnetworksdict) {
					networkselectresults = [];
				}
			}
			$scope.results = networkselectresults;
			$scope.filterrowflag = true;
			$scope.gridOptions.data = $scope.results; //update the data in grid
			//$scope.gridApi.core.refresh(); //refresh the grid
		};

		/*******************************************************************************************
		 * isMarkerInsidePolygon function allows to know if the marker is inside in the polygon draw 
		 *******************************************************************************************/
		$scope.isMarkerInsidePolygon = function (marker, poly) {
			var polyPoints = poly.getLatLngs();
			var x = marker.getLatLng().lat,
				y = marker.getLatLng().lng;

			var inside = false;
			for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
				var xi = polyPoints[i].lat,
					yi = polyPoints[i].lng;
				var xj = polyPoints[j].lat,
					yj = polyPoints[j].lng;

				var intersect = ((yi > y) !== (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
				if (intersect) {
					inside = !inside;
				}
			}

			return inside;
		};

		/******************************************************************
		 * changeRadiusCircle function handles the changing radius of circle
		 *******************************************************************/
		$scope.changeRadiusCircle = function () {
			if (($scope.latitude !== '') && ($scope.longitude !== '')) {
				$scope.drawcircle = 'draw';
				$scope.mymap.removeLayer($scope.layer);
				$scope.layer = L.circle([parseFloat($scope.latitude), parseFloat($scope.longitude)], $scope.Radius * 1000);
				$scope.layer.addTo($scope.mymap);
				var theCenterPt = [parseFloat($scope.latitude), parseFloat($scope.longitude)];
				$scope.latLong = theCenterPt;
				var theRadius = $scope.Radius * 1000;
				var newDataList = [];

				$scope.mymap.eachLayer(function (icon) {
					if (icon.hasOwnProperty("_latlng")) {
						var layerlatlong = icon.getLatLng();
						var distancefromcenterPoint = layerlatlong.distanceTo(theCenterPt);
						if ((distancefromcenterPoint <= theRadius) && (icon.result !== undefined)) {
							newDataList.push(icon.result);
						}
					}
				});
				$scope.filterrowflag = false;
				$scope.gridOptions.data = newDataList;
				$scope.gridApi.core.refresh();
			}
		};

		/****************************************************************
		 * changeRectangle function handles the changing size of rectangle
		 ****************************************************************/
		$scope.changeRectangle = function () {
			if (($scope.South !== '') && ($scope.North !== '') && ($scope.East !== '') && ($scope.West !== '')) {
				$scope.drawrectangle = 'draw';
				$scope.mymap.removeLayer($scope.layer);
				var middlelat = (parseFloat($scope.South) + parseFloat($scope.North)) / 2;
				var middlelon = (parseFloat($scope.East) + parseFloat($scope.West)) / 2;
				$scope.latLong = [middlelat, middlelon];
				var bounds = L.latLngBounds([$scope.North, $scope.West], [$scope.South, $scope.East]);
				$scope.layer = L.rectangle(bounds);
				$scope.layer.addTo($scope.mymap);
				$scope.mymap.fitBounds($scope.layer.getBounds());
				var newDataList = [];
				$scope.mymap.eachLayer(function (icon) {
					if (icon.hasOwnProperty("_latlng")) {
						var layerlatlong = icon.getLatLng();

						if (bounds.contains(layerlatlong)) {
							newDataList.push(icon.result);
						}
					}
				});
				$scope.filterrowflag = false;
				$scope.gridOptions.data = newDataList;
				$scope.gridApi.core.refresh();
			}
		};

		/*****************************************************
		 * simple function handles the on click "simple" button
		 ******************************************************/
		$scope.simple = function () {
			$scope.simpleoradvanced = 'simple';
			$scope.selected = !$scope.selected;
			$scope.receivertypeselected = '';
			$scope.antennatypeselected = '';
			$scope.radometypeselected = '';
			$scope.satellitesystemselected = '';
			$scope.filefromdateselected = '';
			$scope.filetodateselected = '';
			$scope.publishfromdateselected = '';
			$scope.publishtodateselected = '';
			$scope.dataavailabilityselected = '';
			$scope.stationlifetimeselected = '';
			$scope.datasamplingselected = '';
			$scope.filetypeselected = '';
			$scope.statusfileselected = '';
			$scope.dataqualityselected = '';
			$scope.satellitesystemt3selected = '';
			$scope.filelatencyselected = '';
			$scope.typeofsignalselected = '';
			$scope.frequencyselected = '';
			$scope.channelselected = '';
			$scope.epochselected = '';
			$scope.elevangleselected = '';
			$scope.multipathselected = '';
			$scope.cycleslipsselected = '';
			$scope.ssprmsselected = '';
			$scope.clockjumpselected = '';
		};
		/*********************************************************
		 * advanced function handles the on click "advanced" button
		 **********************************************************/
		$scope.advanced = function () {
			$scope.simpleoradvanced = 'advanced';
			$scope.selected = !$scope.selected;
			$scope.loadAdvanced();
		};


		/**************************************************************************
		 * routeNetworklink function handles on click links to access network detail
		 **************************************************************************/
		$scope.routeNetworklink = function (network_name) {
			var network_name_repl = network_name.replace(" ", ",");
			//open  the view network with the parameter network
			var mypass = $window.open('#/network/' + network_name_repl, '_blank');
		};

		/*****************************************************************************
		 * splitNetworklink function allows to split network name when there're several
		 *****************************************************************************/
		$scope.splitNetworklink = function (network_name) {
			//var res = network_name.replace('& ', '');
			var res = network_name.split(' & ');
			return res;
		};

		/*****************************************************************************
		 * cookieTeamReduced function allows to add cookie when user ckecked don't
		 * show again
		 *****************************************************************************/
		$scope.cookieTeamReduced = function (event) {
			var cookies = $cookies.getAll();
			var add = false;
			angular.forEach(cookies, function (v, k) {
				if (k == 'TeamReduced') {
					console.log('always in cookies')
					if (event.currentTarget.checked == false) {
						console.log('remove from cookies')
						$cookies.remove(k);
					}
				} else {
					console.log("not in cookies");
					if (event.currentTarget.checked == true) {
						add = true;
					}
				}
				console.log(v, k)
			});
			if (add === true) {
				let expire = new Date();
				expire.setDate(expire.getDate() + 7);
				console.log(`put key: 'TeamReduced' and value ${event.currentTarget.checked} from cookies`)
				$cookies.put('TeamReduced', event.currentTarget.checked, { expires: expire });

			}
		}

		/**
		 * Show a message on the map explaining how to zoom
		 */
		$scope.showZoomMessage = function () {
			L.Control.zoomText = L.Control.extend({
				onAdd: function (map) {
					var text = L.DomUtil.create('div');
					text.id = "zoomText";
					text.innerHTML = "<strong>Zoom : CTRL+SCROLL</strong>";
					return text;
				},
				onRemove: function (map) { }
			});
			L.control.zoomText = function (opts) { return new L.Control.zoomText(opts); }
			L.control.zoomText({ position: 'bottomright' }).addTo($scope.mymap);
		}

		/**
		 * Create network.json which allow us to give colors and shape to markers
		 */
		$scope.createNetworkJSON = function (data) {
			var json = [];
			var registered = [];
			data.forEach(elem => {
				var values = $scope.makeTabFromStr(elem.network.name);
				values.forEach(ntwk => {
					var type = "local"	// we assume every network is local
					var color = "#" + ('00000' + (Math.random() * (1 << 24) | 0).toString(16)).slice(-6);
					if (registered.indexOf(ntwk) === -1) {
						if (["VOLC"].indexOf(ntwk) >= 0 || ntwk.includes("NFO_") || ntwk.includes("E-GVAP")) { // check if it is a virtual network
							type = "virtual";
							if (ntwk == "VOLC") {
								color = "#00AAFF";
							} else if (ntwk.includes("NFO_")) {
								color = "#00EEFF";
								ntwk = "NFO";
							} else if (ntwk == "E-GVAP") {
								color = "#DDD"
							}
						} else if (["EPN", "IGS", "EPOS"].indexOf(ntwk) >= 0) { // check if it is an international network
							type = "international";
							if (ntwk === "IGS") {
								color = "#FF0000";
							} else {
								color = "#EE9900";
							}
						}
						var obj = {
							"network": ntwk,
							"color": color,
							"type": type
						};
						json.push(obj);
						registered.push(ntwk);
					}
				});
			});
			$scope.json = json;
		}

		$scope.loadResults();
	}
	);